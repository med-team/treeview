/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: MainPanel.java,v $
 * $Revision: 1.12 $
 * $Date: 2010-05-02 13:33:30 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;

import javax.swing.ImageIcon;

/**
 *  implementing objects are expected to be subclasses of component. The purpose
 *  of this class is to provide an interface for LinkedView, whereby different views
 *  can be added to a tabbed panel. This is meant to eventually become a plugin interface.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version $Revision: 1.12 $ $Date: 2010-05-02 13:33:30 $
 */
public interface MainPanel {
	
	/**
	 *  This syncronizes the sub compnents with their persistent storage.
	 */
	public void syncConfig();

	/**
	* this method gets the config node on which this component is based, or null.
	*/
	public ConfigNode getConfigNode();

	/**
	 *  Add items related to settings
	 *
	 * @param  menubar  A menu to add items to.
	 */
	public void populateSettingsMenu(TreeviewMenuBarI menubar);


	/**
	 *  Add items which do some kind of analysis
	 *
	 * @param  menubar  A menu to add items to.
	 */
	public void populateAnalysisMenu(TreeviewMenuBarI menubar);


	/**
	 *  Add items which allow for export, if any.
	 *
	 * @param  menubar  A menu to add items to.
	 */
	public void populateExportMenu(TreeviewMenuBarI menubar);


	/**
	 *  ensure a particular gene is visible. Used by Find.
	 *
	 *	The index is relative to the shared TreeSelection object
	 * associated with the enclosing ViewFrame.
	 *
	 * @param  i  Index of gene to make visible
	 */
	public void scrollToGene(int i);
	public void scrollToArray(int i);
	
	/**
	 * 
	 * @return name suitable for displaying in tab
	 */
	public String getName();
	
	/**
	 *
	 * @return Icon suitable for putting in tab, or in minimized window.
	 */
	public ImageIcon getIcon();
	
	/**
	 * This exists to allow plugins to have scripted output to image files.
	 * This function is triggered by a -x PluginName argument to the main app.
	 * 
	 * @param argv
	 * @throws ExportException 
	 */
	public void export(MainProgramArgs args) throws ExportException;
	
}

