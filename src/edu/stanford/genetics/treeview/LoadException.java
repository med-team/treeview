/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: LoadException.java,v $
 * $Revision: 1.6 $
 * $Date: 2007-02-03 04:58:36 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


public class LoadException extends Exception {// errors when loading model
    public static final int EXT = 0; // Wrong extension, not cdt
    public static final int CDTPARSE = 1; // cdt parse error
    public static final int ATRPARSE = 2; // atr parse error
    public static final int GTRPARSE = 3; // gtr parse error
    public static final int INTPARSE = 4; // parse interrupted
    public static final int KAGPARSE = 5; // kag parse error
    public static final int KGGPARSE = 6; // kgg parse error
    public static final int NOFILE = 7;   // no file selected
    int type;
    public LoadException(String message, int t) {
	super(message);
	type = t;
    }
    public int getType() {
	return type;
    }
	public String getMessage() {
		return "LoadException " + type + ": " + super.getMessage();
	}
}
