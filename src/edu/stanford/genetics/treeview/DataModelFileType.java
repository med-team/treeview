package edu.stanford.genetics.treeview;

public enum DataModelFileType {
	CDT, GTR, ATR;
}
