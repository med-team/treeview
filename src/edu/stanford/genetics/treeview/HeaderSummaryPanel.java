/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: HeaderSummaryPanel.java,v $
 * $Revision: 1.9 $
 * $Date: 2005-12-05 05:27:53 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;

import java.util.Observable;
import java.util.Observer;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
/**
* enables editing of a headerSummary object.
*/
public class HeaderSummaryPanel extends JPanel  implements SettingsPanel,Observer {
		private HeaderInfo headerInfo;
	/** Setter for headerInfo */
	public void setHeaderInfo(HeaderInfo headerInfo) {
		if (this.headerInfo != null) this.headerInfo.deleteObserver(this);
		this.headerInfo = headerInfo;
		headerInfo.addObserver(this);
		synchronizeFrom();
	}
	/** Getter for headerInfo */
	public HeaderInfo getHeaderInfo() {
		return headerInfo;
	}
	
	
	
	private HeaderSummary headerSummary;
	/** Setter for headerSummary */
	public void setHeaderSummary(HeaderSummary headerSummary) {
		this.headerSummary = headerSummary;
		synchronizeFrom();
	}
	/** Getter for headerSummary */
	public HeaderSummary getHeaderSummary() {
		return headerSummary;
	}
	private JList headerList = new JList(new String [0]);
	/** Setter for headerList */
	public void setHeaderList(String [] headers) {
		if (headers == null) {
			headerList.setListData(new String [0]);
		} else {
			headerList.setListData(headers);
		}
	}
	/** Getter for headerList */
	public JList getHeaderList() {
		return headerList;
	}
	
	public void synchronizeFrom() {
			int [] included = getHeaderSummary().getIncluded();
			JList list = getHeaderList();
			if (list == null) return;
			list.clearSelection();
			for (int i = 0; i < included.length; i++) {
				int index = included[i];
				if ((index >=0) && (index < list.getModel().getSize())) {
					list.addSelectionInterval(index,index);
				}
			}
	}
	public void synchronizeTo() {
		getHeaderSummary().setIncluded(getHeaderList().getSelectedIndices());
	}
	
	public HeaderSummaryPanel(HeaderInfo headerInfo, HeaderSummary headerSummary) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.headerInfo = headerInfo;
		this.headerSummary = headerSummary;

		add(new JLabel("Headers to include"));
		setHeaderList(headerInfo.getNames());
		headerList.setVisibleRowCount(5);
		add(new JScrollPane(getHeaderList()));
		ListSelectionListener tmp = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				synchronizeTo();
			}
		};
		getHeaderList().addListSelectionListener(tmp);
		synchronizeFrom();
	}

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(Observable o, Object arg) {
		if (o == headerInfo) {
			setHeaderList(headerInfo.getNames());
			synchronizeFrom();
			repaint();
		} else {
			LogBuffer.println("HeaderSummaryPanel got update from unexpected observable " + o);
		}
	}
}