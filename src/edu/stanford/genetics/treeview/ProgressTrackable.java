/*
 *  BEGIN_HEADER                                              Java TreeView
 *
 *  $Author: alokito $
 *  $RCSfile: ProgressTrackable.java,v $
 *  $Revision: 1.2 $
 *  $Date: 2007-02-03 05:20:09 $
 *  $Name:  $
 *
 *  This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *  END_HEADER
 */
 
 package edu.stanford.genetics.treeview;

/**
 * The idea here is that objects which implement this interface can have their progress depicted
 *by a progress bar. 
 *
 * Typically, classes that implement this interface will use a LoadProgress2 
 * instance to actually display their progress, and will manage the "phases"
 * portion of their behavior. However, they will pass off a pointer to themselves,
 * cast to ProgressTrackable, to various routines within the phase that will
 * then update the within-phase scrollbar.
 *
 * The purpose of this interface is to allow these slow running subroutines to 
 * communicate back to the object that manages the phases of the long running 
 * task, which in turn manages the LoadProgress instance.
 *
 * @author     aloksaldanha
 */
public interface ProgressTrackable {

	/**
	* The length holds the length in bytes of the input stream, or -1 if not known.
	*/
	/** Getter for length */
	public int getLength();
	
	/**
	 * Enables classes to which we delegate loading
	 * to update the length of the progress bar
	 * 
	 * @param i
	 */
	public void setLength(int i);
	
	/**
	* The value holds the current position, which will be between 0 and length, if length is >= 0.
	*/
	/** Getter for value */
	public int getValue();
	/**
	 * sets value of scrollbar
	 * @param i
	 */
	public void setValue(int i);
	/**
	 * increments scrollbar by fixed amount
	 * @param i
	 */
	public void incrValue(int i);

	/**
	 * 
	 * @return true if this task has been cancelled.
	 */
	public boolean getCanceled();

}
