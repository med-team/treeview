/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: MessagePanel.java,v $
 * $Revision: 1.6 $
 * $Date: 2004-12-21 03:28:13 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import java.awt.*;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.*;

public class MessagePanel extends JScrollPane {
    
    protected MessageCanvas messagecanvas;
    protected Vector messages;

    private String title;

		class MessageCanvas extends JPanel {
			
			public void paintComponent(Graphics g) {
				
				int xoff = 0;
				FontMetrics metrics = getFontMetrics(g.getFont());
				int ascent = metrics.getAscent();
				int height = 0;
				Enumeration e = messages.elements();
				Dimension size = getSize();
				g.clearRect(0, 0, size.width, size.height);
				
				height += ascent;
				g.drawString(title,-xoff, height);

				
				while (e.hasMoreElements()) {
					String message = (String) e.nextElement();
					if (message == null) continue;
					height += ascent;
					g.drawString(message, -xoff, height);
				}
			}
			
			public Dimension getPreferredSize() {
				FontMetrics metrics = getFontMetrics(getFont());
				int ascent = metrics.getAscent();
				// for title...
				int height = ascent;
				int width = metrics.stringWidth(title);
				Enumeration e = messages.elements();
				while (e.hasMoreElements()) {
					String message = (String) e.nextElement();
					if (message == null) continue;
					height += ascent;
					int length = metrics.stringWidth(message);
					if (width < length) {width = length;}
				}
				return new Dimension(width, height);
			}
			
		}	
		
		public MessagePanel() {
			this(null);
		}

	public MessagePanel(String t) {
		super();
		title = t;
		messages = new Vector(5,5);
		messagecanvas = new MessageCanvas();
		messagecanvas.setBackground(Color.white);
		messagecanvas.setForeground(Color.black);
		setViewportView(messagecanvas);
	}
    
		public void setMessages(String [] m) {
			resetMessages();
			int i;
			for (i = 0; i < m.length; i++) {
				addMessage(m[i]);
			}
			layoutMessages();
		}

		public void resetMessages() {
			messages.removeAllElements();
		}
		
		public void addMessage(String message) {
			messages.addElement(message);
		}
		
		public void layoutMessages() {
			revalidate();
			repaint();
		}
}
