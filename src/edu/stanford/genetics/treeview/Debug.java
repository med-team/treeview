/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: Debug.java,v $
 * $Revision: 1.6 $
 * $Date: 2006-08-18 06:50:17 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;

import edu.stanford.genetics.treeview.RCSVersion;


/** I, Alok, didn't write this. */
public class Debug {
	public static final boolean on = true;
	static RCSVersion version = new RCSVersion("$Id: Debug.java,v 1.6 2006-08-18 06:50:17 rqluk Exp $, debugging" + (on ? "on" : "off"));

	public static void print(Object caller, String message, Object argument) {
		if(on) {
			String c = (caller == null) ? "" : caller.toString();
			String a = (argument == null) ? "" : argument.toString();

			if(c.length() > 79)
				c = c.substring(0, 79);
			if(a.length() > 77)
				a = a.substring(0, 77);
			System.out.println(c + ":" + message + "(" + a + ")");
		}
	}

	public static void print(Object caller, String message) {
		if(on)
			print(caller, message, null);
	}

	public static void print(String message) {
		if(on)
			print(null, message, null);
	}

}

