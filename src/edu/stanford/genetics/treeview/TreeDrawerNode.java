/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: TreeDrawerNode.java,v $
 * $Revision: 1.8 $
 * $Date: 2005-03-07 22:20:41 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;

/**
 * Represents nodes in ATRView and GTRView.
 *
 * HACK Should really retrofit this so that it's a subclass of DefaultMutableTreeNode. 
 *
 * @author Alok Saldanha <alok@genome.stanford.edu>
 * @version Alpha
 */

import java.awt.Color;
import java.util.Stack;

public class TreeDrawerNode  {
    //accessors
	/**
	 * @return index of node, i.e. where to draw node across width of tree
	 */
    public double getIndex() {return ind;}
	/**
	 * @return correlation of node, i.e. where to draw node across height of tree
	 */
    public double getCorr() {return corr;}
    public String getId() { return id;}
    public TreeDrawerNode getParent() { return parent;}
    public TreeDrawerNode getLeft() { return left;}
    public TreeDrawerNode getRight() { return right;}
    
    // settors
    public void setParent(TreeDrawerNode n) { parent = n;}
		public void setCorr(double newCorr) {corr = newCorr;}
   //    public void setLeft  (TreeDrawerNode n) { left = n;}
    //    public void setRight (TreeDrawerNode n) { right = n;}
		/**
		* returns maximum correlation value (really branch height) for this subtree 
		*/
		public double getMaxCorr() {
			double curCorr = getCorr();
			if (isLeaf())	return curCorr;
			
			Stack remaining = new Stack();
			remaining.push(this);
			while (remaining.empty() == false) {
				TreeDrawerNode node = (TreeDrawerNode) remaining.pop();
				if (node.getCorr() > curCorr)
					curCorr = node.getCorr();

				TreeDrawerNode leftNode = node.getLeft();
				if (leftNode != null) remaining.push(leftNode);

				TreeDrawerNode rightNode = node.getRight();
				if (rightNode != null) remaining.push(rightNode);
			}
			
			return curCorr;
		}

		/**
		 * returns minimum correlation value (really branch height) for this subtree 
		 */
		public double getMinCorr() {
			double curCorr = getCorr();
			if (isLeaf())	return curCorr;
			
			Stack remaining = new Stack();
			remaining.push(this);
			while (remaining.empty() == false) {
				TreeDrawerNode node = (TreeDrawerNode) remaining.pop();
				if (node.getCorr() < curCorr)
					curCorr = node.getCorr();

				TreeDrawerNode leftNode = node.getLeft();
				if (leftNode != null) remaining.push(leftNode);

				TreeDrawerNode rightNode = node.getRight();
				if (rightNode != null) remaining.push(rightNode);
			}
			return curCorr;
		}
		
		/**
		 * This method interatively finds the node with the given id.
		 * @param nodeid ID of the node to be found.
		 * @return the node found, or null if no such node exists
		 */
		public TreeDrawerNode findNode(String nodeid)
		{
			Stack remaining = new Stack();
			remaining.push(this);
			while (remaining.empty() == false) {
				TreeDrawerNode node = (TreeDrawerNode) remaining.pop();
			
				if(node.getId().equals(nodeid)) return node;

				TreeDrawerNode leftNode = node.getLeft();
				if (leftNode != null) remaining.push(leftNode);

				TreeDrawerNode rightNode = node.getRight();
				if (rightNode != null) remaining.push(rightNode);
			}
			
			return null;
			
		}	

    public double getDist(double index, double correlation, double weight) {
	double dx = ind - index;
	double dy = corr - correlation;
	dy *= weight;
	return dx * dx + dy * dy;
    }

    // constructor for leafs
    public TreeDrawerNode (String i, double correlation, double index) {
	id = i;
	corr = correlation;
	ind = index;
	minInd = index;
	maxInd = index;
    }
    // constructor for internal nodes
    public TreeDrawerNode (String i, double correlation, TreeDrawerNode l, TreeDrawerNode r) {
	id = i;
	corr = correlation;
	right = r;
	left = l;
	ind = (right.getIndex() + left.getIndex()) / 2;
	minInd = Math.min(right.getMinIndex(), left.getMinIndex());
	maxInd = Math.max(right.getMaxIndex(), left.getMaxIndex());
	if (minInd > maxInd) {
	    throw new RuntimeException
		("min was less than max! this should not happen.");
	}
    }
    

    public double getMaxIndex() {
	return maxInd;
    }

    public double getMinIndex() {
	return minInd;
    }
    
    public boolean isLeaf() {
	return ((left == null) && (right == null));
    }
        
    public TreeDrawerNode getLeftLeaf() {
    		TreeDrawerNode cand = this;
    		while (cand.isLeaf() == false)
    			cand = cand.getLeft();
    		return cand;
    }
    public TreeDrawerNode getRightLeaf() {
		TreeDrawerNode cand = this;
		while (cand.isLeaf() == false)
			cand = cand.getRight();
		return cand;
    }
    
    public double getRange() {
	return maxInd - minInd;
    }

    public Color getColor() {
	return color;
    }

    public void setColor(Color c) {
	color = c;
    }
		
	public void printSubtree() {
		printRecursive("");
	}
	private void printRecursive(String pre) {
		if (getLeft() != null) {
			System.out.println(pre + getId() + ", corr " + getCorr() +", index " + getIndex());
			System.out.println(pre + "Left:");
			getLeft().printRecursive(pre + " ");
			System.out.println(pre + "Right:");
			getRight().printRecursive(pre + " ");
		} else {
			System.out.println(pre + getId() + " LEAF, corr " + getCorr() +", index " + getIndex());
		}
		
	}
	

    // instance variables
    private double corr = 0.0;
    private double ind = -1;
    private double minInd, maxInd; // store max and min ind from this one.
    private TreeDrawerNode parent = null;
    private TreeDrawerNode left = null;
    private TreeDrawerNode right = null;
    private String id = null;
    private Color color = Color.black;
}
