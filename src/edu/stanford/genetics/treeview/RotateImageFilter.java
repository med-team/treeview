/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: RotateImageFilter.java,v $
 * $Revision: 1.3 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import java.awt.*;
import java.awt.image.*;

public class RotateImageFilter {
    // this could probably be generalized if necessary...
    public static Image rotate(Component c,Image in) {
	int width = in.getWidth(null);
	int height = in.getHeight(null);
	if (width < 0) return null;
	int imgpixels[] = new int[width * height];
	int npixels[] = new int[width * height];
	try {
	    PixelGrabber pg = new PixelGrabber(in, 0, 0, width, height, 
					       imgpixels, 0, width);
	    pg.grabPixels();
	} catch (java.lang.InterruptedException e) {
	    System.out.println("Intterrupted exception caught...");
	}
	for (int j = 0; j < height; j++) {
	    for (int i = 0; i < width ; i ++) {
		npixels[j + (width - i - 1)* height] =
		    imgpixels[i + j * width];
	    }
	}
	
	return c.createImage
	    (new MemoryImageSource(height, width, npixels, 0, height));
    }
}

