/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: UrlExtractor2.java,v $
 * $Revision: 1.3 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


/** This is the second url extraction class I'm writing.  It's
 * designed to do less... I'm going to make UrlPresets and UrlEditor
 * as well.  
 */

public class UrlExtractor2 implements ConfigNodePersistent {    
    private ConfigNode root;
    private UrlPresets presets;
    /**
     * This class must have a config node to stash data in, even if
     * it's a dummy. It also needs UrlPresets to infer templates from styles
     */
    public UrlExtractor2(ConfigNode n, UrlPresets p) {
	root = n;
	presets = p;
    }
    
    /**
     * returns the text of the header which is to be used for
     * filling out the url template.
     */
    public String getColHeader() {
	String ret = root.getAttribute("header", null);
	return ret;
    }

    public void setColHeader(String head) {
	root.setAttribute("header", head, null);
    }
    
    /**
     * most common use, fills in current template with val
     */
    public String substitute(String val) {
	String temp = getTemplate();
	if (temp == null) return null;
	if (val == null) return null;
	int into = temp.indexOf("HEADER");
	if (into < 0) return temp;	    
	return temp.substring(0, into) + val + temp.substring(into+6);
    }
    
    public String getTemplate() {
	String ret = root.getAttribute("template", null);
	if (ret != null) return ret;
	
	// try style preset
	if (ret == null)
	    ret = presets.getTemplate(root.getAttribute("style", "None"));

	// try custom
	if (ret == null) 
	    ret = root.getAttribute("custom", null);

	// okay, first preset...
	if (ret == null) 
	    ret = presets.getTemplate(0);
	
	root.setAttribute("template", ret, null);
	return ret;
    }

    public void setTemplate(String ret) {
	root.setAttribute("template", ret, null);
    }


    public void bindConfig(ConfigNode configNode)
    {
        root = configNode;
    }
}
