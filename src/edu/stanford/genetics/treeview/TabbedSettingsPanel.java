/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: TabbedSettingsPanel.java,v $
 * $Revision: 1.7 $
 * $Date: 2008-06-11 01:58:57 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import javax.swing.*;
/**
 * This class is actually a settings panel container. You can put multiple settings panels
 * within this container and tab between them. It does not provide a save/cancel button,
 * for that use a SettingsPanelHolder.
 * 
 * @author aloksaldanha
 *
 */
public class TabbedSettingsPanel extends JTabbedPane implements SettingsPanel {

	public void setSelectedIndex(int i) {
		synchronizeFrom(i);
		super.setSelectedIndex(i);
	}
	public void synchronizeTo() {
		int n = getTabCount();
		for (int i = 0; i < n; i++) {
			synchronizeTo(i);
		}
	}
	public void synchronizeTo(int i) {
		((SettingsPanel) getComponentAt(i)).synchronizeTo();
	}
	
	
	
	public void synchronizeFrom() {
		int n = getTabCount();
		for (int i = 0; i < n; i++) {
			((SettingsPanel) getComponentAt(i)).synchronizeFrom();
		}
	}
	public void synchronizeFrom(int i) {
		((SettingsPanel) getComponentAt(i)).synchronizeFrom();
	}
	public void addSettingsPanel(String name, SettingsPanel sP) {
		addTab(name, (java.awt.Component) sP);
	}
}

