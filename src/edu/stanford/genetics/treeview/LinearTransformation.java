/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: LinearTransformation.java,v $
 * $Revision: 1.3 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


/**
 * this class encapuslates a linear equation.
 * I probably could have named it line equation or something...
 *
 */
public class LinearTransformation {
    // y = mx + b
    private double m = 0.0;
    private double b = 0.0;    
    // for the inverse transformation
    private double mi = 0.0;
    private double bi = 0.0;    
    public double getSlope() {
	return m;
    }

    public LinearTransformation (double fromX, double fromY, double toX, double toY) {
	setMapping(fromX, fromY, toX, toY);
	/*	
	System.out.println("New line y = " + m + " x + " + b);
	System.out.println("from (" + fromX + ", " + fromY + "), (" + toX+ ", " + toY + ")");
	*/
    }
    public LinearTransformation () { /* default, map everything to 0.0 */ }
    public void setMapping(double fromX, double fromY, double toX, double toY) {
	m = (toY - fromY)/ (toX - fromX);	
	b = (fromY*toX - toY*fromX) / (toX - fromX);
	mi = (toX - fromX) / (toY - fromY);
	bi = (fromX*toY - toX*fromY) / (toY - fromY);
    }
    public double transform(double y) {
	return (m * y + b);
    }
    public double inverseTransform(double y) {
	return (mi * y + bi);
    }
}
