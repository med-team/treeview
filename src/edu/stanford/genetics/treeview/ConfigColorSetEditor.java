/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: ConfigColorSetEditor.java,v $
 * $Revision: 1.4 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
/**
 *  This class allows editing of a color set.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.4 $ $Date: 2004-12-21 03:28:14 $
 */

public class ConfigColorSetEditor extends JPanel {
	private ConfigColorSet colorSet;


	/**
	 *  Constructor for the ConfigColorSetEditor object
	 *
	 * @param  colorSet  A ConfigColorSet to be edited
	 */
	public ConfigColorSetEditor(ConfigColorSet colorSet) {
		this.colorSet = colorSet;
		String[] types  = colorSet.getTypes();
		for (int i = 0; i < types.length; i++) {
			add(new ColorPanel(i));
		}
	}


	/**
	 *  A simple test program. Allowed editing, and prints out the results.
	 *
	 * @param  argv  none required.
	 */
	public final static void main(String[] argv) {
		String[] types            = new String[]{"Up", "Down", "Left", "Right"};
		String[] colors           = new String[]{"#FF0000", "#FFFF00", "#FF00FF", "#00FFFF"};

		ConfigColorSet temp       = new ConfigColorSet("Bob", types, colors);
		ConfigColorSetEditor cse  = new ConfigColorSetEditor(temp);
		JFrame frame              = new JFrame("ColorSetEditor Test");
		frame.getContentPane().add(cse);
		frame.pack();
		frame.setVisible(true);
	}

	/** this has been superceded by the general ColorPanel class */
	class ColorPanel extends JPanel {
		ColorIcon colorIcon;
		int type;


		ColorPanel(int i) {
			type = i;
			redoComps();
		}


		public void redoComps() {
			removeAll();
			colorIcon = new ColorIcon(10, 10, getColor());
			JButton pushButton  = new JButton(getLabel(), colorIcon);
			pushButton.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Color trial  = JColorChooser.showDialog(ConfigColorSetEditor.this, "Pick Color for " + getLabel(), getColor());
						if (trial != null) {
							setColor(trial);
						}
					}
				});

			add(pushButton);
		}


		private void setColor(Color c) {
			colorSet.setColor(type, c);
			colorIcon.setColor(getColor());
			repaint();
		}


		private String getLabel() {
			return colorSet.getType(type);
		}


		private Color getColor() {
			return colorSet.getColor(type);
		}
	}

}

