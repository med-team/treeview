/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: UrlSettingsPanel.java,v $
 * $Revision: 1.6 $B
 * $Date: 2010-05-11 13:30:43 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * This class displays editable Url settings.
 *
 * It requires a UrlExtractor, HeaderInfo and optionally a UrlPresets
 */
public class UrlSettingsPanel extends JPanel implements SettingsPanel {
	private UrlExtractor urlExtractor;
	private UrlPresets urlPresets = null;
	private HeaderInfo headerInfo;

	private JDialog d;
	private Window window;
	public UrlSettingsPanel(UrlExtractor ue, UrlPresets up) {
		this(ue, ue.getHeaderInfo(), up);
	}
	public UrlSettingsPanel(UrlExtractor ue, HeaderInfo hi, UrlPresets up) {
		super();
		urlExtractor = ue;
		urlPresets = up;
		headerInfo = hi;
		templateField = new TemplateField();
		templateField.setText(urlExtractor.getUrlTemplate());

		redoLayout();
		updatePreview();
		UrlSettingsPanel.this.setEnabled(urlExtractor.isEnabled());

	}

	public static void main(String [] argv) {
		UrlPresets p = new UrlPresets(new DummyConfigNode("UrlPresets"));
		HeaderInfo hi = new DummyHeaderInfo();
		UrlExtractor ue = new UrlExtractor(hi);

		UrlSettingsPanel e  = new UrlSettingsPanel( ue, hi, p);
		Frame f = new Frame("Url Settings Test");
		f.add(e);
		f.addWindowListener(new WindowAdapter (){
			public void windowClosing(WindowEvent we)
			{System.exit(0);}
		});
		f.pack();
		f.setVisible(true);
	}

	public void synchronizeFrom() {
		redoLayout();
		UrlSettingsPanel.this.setEnabled(urlExtractor.isEnabled());
	}

	public void synchronizeTo() {
		//nothing to do...
	}
	private class ModeChoice extends JComboBox implements ItemListener {
		final private UrlExtractor.UrlExtractorMode[] modes;
		ModeChoice() {
			super();
			modes = UrlExtractor.UrlExtractorMode.values();
			for (int i = 0; i < modes.length; i++) {
				addItem(modes[i].toString());
			}
			setSelectedItem(urlExtractor.getUrlExtractorMode().toString());
			addItemListener(this);
		}
		public void itemStateChanged(ItemEvent e) {
			UrlExtractor.UrlExtractorMode mode = modes[this.getSelectedIndex()];
			urlExtractor.setUrlExtractorMode(mode);
			UrlSettingsPanel.this.setEnabled(urlExtractor.isEnabled());
			updatePreview();
		}
	}


	/**
	 *  Create a blocking dialog containing this component
	 *
	 * @param  f  frame to block
	 */
	public void showDialog(Frame f, String title) {
		d = new JDialog(f, title);
		window = d;
		d.setLayout(new BorderLayout());
		d.add(this, BorderLayout.CENTER);
		d.add(new ButtonPanel(), BorderLayout.SOUTH);
		d.addWindowListener(
				new WindowAdapter() {
					public void windowClosing(WindowEvent we) {
						we.getWindow().dispose();
					}
				});
		d.pack();
		d.setVisible(true);
	}
	public void showDialog(Frame f) {
		showDialog(f, "Url Settings Test");
	}

	private JButton[] buttons;
	private JTextField previewField;
	private TemplateField templateField;
	private HeaderChoice headerChoice;


	public void setEnabled(boolean b) {
		templateField.setEnabled(b);
		headerChoice.setEnabled(b);
		previewField.setEnabled(b);
		for (int i = 0; i < buttons.length; i++) {
			if (buttons[i] != null)
				buttons[i].setEnabled(b);

		}
	}

	private GridBagConstraints gbc;
	public void redoLayout() {
		String [] preset;
		preset = urlPresets.getPresetNames();
		int nPresets = preset.length;
		removeAll();
		setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.gridy = 0;
		gbc.gridx = 0;
		gbc.weightx = 100;
		final ModeChoice enableBox = new ModeChoice();
		add(enableBox, gbc);
		gbc.gridx  = 1;
		add(templateField, gbc);
		gbc.gridx  = 2;
		headerChoice  = new HeaderChoice();
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0;
		add(headerChoice, gbc);
		gbc.gridx  = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		previewField = new JTextField("Ex: " + urlExtractor.getUrl(0));
//		  previewField = new JTextField(urlExtractor.substitute(tester));
		previewField.setEditable(false);
		add(previewField, gbc);
		JPanel presetPanel = new JPanel();
		buttons = new JButton[nPresets];
		for (int i = 0; i < nPresets; i++) {
			JButton presetButton = new JButton((urlPresets.getPresetNames()) [i]);
			final int index = i;
			presetButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					templateField.setText(urlPresets.getTemplate(index));
					updatePreview();
				}
			});
			presetPanel.add(presetButton);
			buttons[index] = presetButton;
		}
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 100;
		gbc.weightx = 100;
//		  add(new JScrollPane(presetPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), gbc);
		add(presetPanel, gbc);


		try {
			headerChoice.setSelectedIndex(urlExtractor.getIndex());
		} catch(  java.lang.IllegalArgumentException e) {
		}
	}

	private void updatePreview() {
		urlExtractor.setUrlTemplate(templateField.getText());
		urlExtractor.setIndex(headerChoice.getSelectedIndex());
		previewField.setText("Ex: " + urlExtractor.getUrl(0));
	}



	private class HeaderChoice extends JComboBox implements ItemListener {
		HeaderChoice() {
			super();
			String [] headers;
			int lastI;
			if (headerInfo != null) {
				headers = headerInfo.getNames();
				lastI = headers.length;
				if (headerInfo.getIndex("GWEIGHT") != -1) {
					lastI--;
				}
			} else {
				headers = new String [] {"Dummy1", "Dummy2", "Dummy3"};
				lastI = headers.length;
			}

			for (int i = 0; i < lastI; i++) {
				if (headers[i] == null) {
					addItem("-- NULL --");
				} else {
					addItem(headers[i]);
				}
			}
			addItemListener(this);
		}
		public void itemStateChanged(ItemEvent e) {
			updatePreview();
		}
	}



	private class TemplateField extends JTextField {
		TemplateField () {
			super("enter url template");
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					updatePreview();
				}
			});
		}
	}

	private class ButtonPanel extends JPanel {
		ButtonPanel() {
			JButton save_button = new JButton("Close");
			save_button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					window.setVisible(false);
				}
			});
			add(save_button);

		}
	}
}
