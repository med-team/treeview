/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: UrlExtractor.java,v $
 * $Revision: 1.7 $
 * $Date: 2008-04-23 23:26:48 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;

import java.awt.Frame;
import java.io.UnsupportedEncodingException;
/**
 * This class extracts Urls from HeaderInfo.
 * Also included is a class to pop up a configuration window.
 */
public class UrlExtractor {
    /**
     * This class must be constructed around gene header info
     */
    public UrlExtractor(HeaderInfo hI) {
        headerInfo = hI;
        urlTemplate = dUrlTemplate;
        index  = dindex;
        urlExtractorMode = defaultUrlExtractorMode;
        uPresets = null;
    }

    public UrlExtractor(HeaderInfo hI, UrlPresets uPresets) {
        this(hI);
        this.uPresets = uPresets;
        setDefaultTemplate(uPresets.getDefaultTemplate());
        setDefaultUrlExtractorMode(uPresets.isDefaultEnabled()?UrlExtractorMode.TEMPLATE:UrlExtractorMode.DISABLED);
    }

    /**
     * can be bound to config node to provide persistence
     */
    public void bindConfig(ConfigNode n) {
        root = n;
        // extract state...
        urlTemplate = root.getAttribute("urlTemplate" , dUrlTemplate);
        index  = root.getAttribute("index"  , dindex);
        urlExtractorMode = UrlExtractorMode.valueOf(root.getAttribute("mode", defaultUrlExtractorMode.toString()));
    }

    /**
     * most common use, returns a String rep of a url given an index
     * returns null if not enabled, or if the header for this gene is null.
     */
    public String getUrl(int i) {
        if (getUrlExtractorMode() == UrlExtractorMode.DISABLED) return null;
        String [] headers = headerInfo.getHeader(i);
        if (headers == null)
            return null;
        String headerValue = headers[index];
        if (getUrlExtractorMode() == UrlExtractorMode.HEADER) return headerValue;
        return substitute(urlTemplate, headerValue);

    }

    public String getUrl(int i, String header) {
        if(uPresets == null) return null;
        if (getUrlExtractorMode() == UrlExtractorMode.DISABLED) return null;
        String [] headers = headerInfo.getHeader(i);
        if (headers == null) return null;
        String headerValue = headers[index];
        if (getUrlExtractorMode() == UrlExtractorMode.HEADER) return headerValue;
        String tmpTemplate = uPresets.getTemplateByHeader(header);
        if (tmpTemplate == null) tmpTemplate = urlTemplate;
        return substitute(tmpTemplate, headerValue);
    }

    public String substitute(String val) {
        return substitute(urlTemplate, val);
    }
    private String substitute(String temp, String val) {
        if (val == null) return null;
        int into = temp.indexOf("HEADER");
        if (into < 0) return temp;
        try {
            return temp.substring(0, into) + java.net.URLEncoder.encode(val, "UTF-8") + temp.substring(into+6);
        } catch (UnsupportedEncodingException e) {
            LogBuffer.println("unsupported encoding? this shouldn't happen. " + e);
            e.printStackTrace();
            return temp;
        }
    }

    /**
     * pops up a configuration dialog.
     */
    public void showConfig(Frame f) {
        // deprecated...
    }
    //accessors
    public void setIndex(int i) {
        index = i;
        if (root != null)
            root.setAttribute("index", index, dindex);
    }
    public int getIndex() {
        return index;
    }

    public void setUrlTemplate(String c) {
        urlTemplate = c;
        if (root != null)
            root.setAttribute("urlTemplate", urlTemplate, dUrlTemplate);
    }
    public String getUrlTemplate() {
        return urlTemplate;
    }

    public void setDefaultTemplate(String temp) {
        dUrlTemplate = temp;
    }
    public void setDefaultIndex(int i) {
        dindex = i;
    }

    public void setDefaultUrlExtractorMode(UrlExtractorMode b) {
        defaultUrlExtractorMode = b;
    }

    public void setUrlExtractorMode(UrlExtractorMode b) {
        urlExtractorMode = b;
        if (root != null)
            root.setAttribute("mode", urlExtractorMode.toString(), defaultUrlExtractorMode.toString());

    }
    public boolean isEnabled() {
        return urlExtractorMode != UrlExtractorMode.DISABLED;
    }
    public UrlExtractorMode getUrlExtractorMode() {
        return urlExtractorMode;
    }
    // does the user actually want linking to happen?
    private UrlExtractorMode urlExtractorMode;
    private UrlExtractorMode defaultUrlExtractorMode = UrlExtractorMode.TEMPLATE;

    // durlTemplate is the actual text of the url to be substituted
    private String urlTemplate;
    private String dUrlTemplate = "http://www.google.com/search?q=HEADER";

    // the index is the header column of the cdt/pcl which is used for substitution
    private int index;
    private int dindex  = 1;

    private HeaderInfo headerInfo;
    /** Setter for headerInfo */
    public void setHeaderInfo(HeaderInfo headerInfo) {
        this.headerInfo = headerInfo;
    }
    /** Getter for headerInfo */
    public HeaderInfo getHeaderInfo() {
        return headerInfo;
    }
    private ConfigNode root;
    UrlPresets uPresets;

    public enum UrlExtractorMode {
        DISABLED,
        TEMPLATE,
        HEADER
    }
}
