/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: ColorIcon.java,v $
 * $Revision: 1.5 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;
import java.awt.*;

import javax.swing.Icon;

/**
 *  A little icon with a changeable color.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.5 $ $Date: 2004-12-21 03:28:14 $
 */
public class ColorIcon implements Icon {
	private int width, height;
	private Color color;


	/**
	 * @param  x  width of icon
	 * @param  y  height of icon
	 * @param  c  Initial color of icon.
	 */
	public ColorIcon(int x, int y, Color c) {
		width = x;
		height = y;
		color = c;
	}


	/**
	 *  Sets the color, but doesn't redraw or anything.
	 *
	 * @param  c  The new color
	 */
	public void setColor(Color c) {
		color = c;
	}

	/* inherit description */
	public int getIconHeight() {
		return height;
	}


	/* inherit description */
	public int getIconWidth() {
		return width;
	}


	/* inherit description */
	public void paintIcon(Component c, Graphics g, int x, int y) {
		Color old  = g.getColor();
		g.setColor(color);
		g.fillRect(x, y, width, height);
		g.setColor(Color.black);
		g.drawRect(x, y, width, height);
		g.setColor(old);
	}
}

