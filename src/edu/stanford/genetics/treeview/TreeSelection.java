/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: TreeSelection.java,v $
 * $Revision: 1.5 $
 * $Date: 2006-03-20 06:18:43 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;

import java.util.Observable;

/**
 * A quasi-model independant selection model for leaf indexes as well as internal nodes of  trees.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.5 $ $Date: 2006-03-20 06:18:43 $
 */
public class TreeSelection extends Observable implements TreeSelectionI {
	private IntegerSelection integerSelection;
	private String selectedNode;


	/**
	 *  Constructor for the TreeSelection object
	 *
	 * @param  nIndex  number of indexes which can be selected
	 */
	public TreeSelection(int nIndex) {
		integerSelection = new IntegerSelection(nIndex);
	}
	
	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#resize(int)
	 */
	public void resize(int nIndex)  {
		IntegerSelection temp = new IntegerSelection(nIndex);
		
		for(int i = 0; i < nIndex; i++)
		{
			if(i < integerSelection.getNSelectable())
			{
				temp.set(i, integerSelection.isSelected(i));
			}
			else
			{
				temp.set(i , false);
			}
		}
		integerSelection = temp;
		setChanged();
	}

	// index methods
	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#deselectAllIndexes()
	 */
	public void deselectAllIndexes() {
		integerSelection.deselectAll();
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#selectAllIndexes()
	 */
	public void selectAllIndexes() {
		integerSelection.selectAll();
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#setIndex(int, boolean)
	 */
	public void setIndex(int i, boolean b) {
		integerSelection.set(i, b);
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#isIndexSelected(int)
	 */
	public boolean isIndexSelected(int i) {
		return integerSelection.isSelected(i);
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#getMinIndex()
	 */
	public int getMinIndex() {
		return integerSelection.getMin();
	}
	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#getSelectedIndexes()
	 */
	public int [] getSelectedIndexes() {
		return integerSelection.getSelectedIndexes();
	}

	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#getMaxIndex()
	 */
	public int getMaxIndex() {
		return integerSelection.getMax();
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#getNumIndexes()
	 */
	public int getNumIndexes() {
		return integerSelection.getNSelectable();
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#selectIndexRange(int, int)
	 */
	public void selectIndexRange(int min, int max) {
		if (min > max) {
			int swap  = min;
			min = max;
			max = swap;
		}
		for (int i = min; i <= max; i++) {
			integerSelection.set(i, true);
		}
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#getNSelectedIndexes()
	 */
	public int getNSelectedIndexes() {
		return integerSelection.getNSelected();
	}

	// node methods
	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#setSelectedNode(java.lang.String)
	 */
	public void setSelectedNode(String n) {
		if (selectedNode != n) {
			selectedNode = n;
			setChanged();
		}
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.TreeSelectionI#getSelectedNode()
	 */
	public String getSelectedNode() {
		return selectedNode;
	}


	/**
	* a class to efficiently model a range of integers which can be selected.
	*/
	class IntegerSelection {
		boolean[] isSelected;


		IntegerSelection(int size) {
			isSelected = new boolean[size];
			deselectAll();
		}


		public int getNSelectable() {
			return isSelected.length;
		}


		public int getNSelected() {
			int n  = 0;
			for (int i = 0; i < isSelected.length; i++) {
				if (isSelected[i]) {
					n++;
				}
			}
			return n;
		}
		public int [] getSelectedIndexes() {
			int nSelected = getNSelected();
			int [] indexes = new int [nSelected];
			int curr = 0;
			for (int i = 0; i < isSelected.length; i++) {
				if (isSelected[i]) {
					indexes[curr++] = i;
				}
			}
			return indexes;
		}

		public void deselectAll() {
			TreeSelection.this.setChanged();
			for (int i = 0; i < isSelected.length; i++) {
				isSelected[i] = false;
			}
		}


		public void selectAll() {
			TreeSelection.this.setChanged();
			for (int i = 0; i < isSelected.length; i++) {
				isSelected[i] = true;
			}
		}


		public void set(int i, boolean b) {
			if ((i >= 0) && (i < isSelected.length)) {
				TreeSelection.this.setChanged();
				isSelected[i] = b;
			}
		}


		public boolean isSelected(int i) {
			return isSelected[i];
		}


		public int getMin() {
			int min  = -1;
			for (int i = 0; i < isSelected.length; i++) {
				if (isSelected[i]) {
					return i;
				}
			}
			return min;
		}


		public int getMax() {
			int max  = -1;
			for (int i = 0; i < isSelected.length; i++) {
				if (isSelected[i]) {
					max = i;
				}
			}
			return max;
		}
	}
}

