/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: DataModel.java,v $
 * $Revision: 1.18 $
 * $Date: 2008-03-09 21:06:34 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;

/**
 *  This file defines the bare bones of what needs to implemented by a data model
 *  which wants to be used with a ViewFrame and some ModelViews.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.18 $ $Date: 2008-03-09 21:06:34 $
 */

public interface DataModel {
    public final static double NODATA = -10000000;
	public final static double EMPTY = -20000000;

	/**
	 *  Gets the documentConfig attribute of the DataModel object
	 *
	 * values stored in the <code>ConfigNode</code>s of this <code>XmlConfig</code> should be persistent across multiple openings of this DataModel.
	 *
	 *<p>
	 *  Of course, if you don't care about persistence you could subclass XmlConfig to create one which doesn't store things to file.
	 *
	 * @return    The documentConfig value
	 */
	public ConfigNode getDocumentConfigRoot();


	/**
	 *  Gets the file path or url which this <code>DataModel</code> was built from.
	 *
	 * @return    String representation of file path or url
	 */
	public String getSource();

	/**
	 *  Gets a short name, unique for this <code>DataModel</code>, suitable for putting in a windo menu.
	 *
	 * @return    Short name of data model.
	 */
	public String getName();

	
	/**
	 * Sets an data model to be compare to this model.
	 * @param dm The data model.
	 */
	public void setModelForCompare(DataModel dm);


	/**
	 *  Gets the fileSet which this <code>DataModel</code> was built from.
	 *
	 * @return    The actual <code>Fileset</code> which generated this <code>DataModel</code>
	 */
	public FileSet getFileSet();
	public void clearFileSetListeners();
	public void addFileSetListener(FileSetListener listener);


	/**
	 *  Gets the HeaderInfo associated with genes for this DataModel.
	 *
	  * There are two special indexes, YORF and NAME, which mean the unique id column and the description column, respectively. See TVModel.TVModelHeaderInfo for details.
	  */
	public HeaderInfo getGeneHeaderInfo();


	/**
	 *  Gets the HeaderInfo associated with arrays for this DataModel.
	 */
	public HeaderInfo getArrayHeaderInfo();

	/**
	 *  Gets the HeaderInfo associated with gene tree for this DataModel.
	 *
	  * There are two special indexes, YORF and NAME, which mean the unique id column and the description column, respectively. See TVModel.TVModelHeaderInfo for details.
	  */
	public HeaderInfo getGtrHeaderInfo();


	/**
	 *  Gets the HeaderInfo associated with array tree for this DataModel.
	 */
	public HeaderInfo getAtrHeaderInfo();


	/**
	 *  This not-so-object-oriented hack is in those rare instances where it is not
	 *  enough to know that we've got a DataModel.
	 *
	 * @return    a string representation of the type of this <code>DataModel</code>
	 */
	public String getType();


	/**
	* returns the datamatrix which underlies this data model,
	* typically the matrix of measured intensity ratios.
	*/
	public DataMatrix getDataMatrix();
	
	
	void append(DataModel m);

	/**
	 * Removes the previously appended DataMatrix.
	 *
	 */
	void removeAppended();


	/**
	 * @return
	 */
	public boolean aidFound();


	/**
	 * @return
	 */
	public boolean gidFound();


	/**
	 * @return true if data model has been modified since last save to source.
	 * always returning false is generally a safe thing, if you have an
	 * immutable data model.
	 */
	public boolean getModified();

	/**
	 * 
	 * @return true if data model has been sucessfully loaded.
	 */
	public boolean isLoaded();
}

