/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: TestViewFrame.java,v $
 * $Revision: 1.15 $
 * $Date: 2009-08-26 11:48:27 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;
import javax.swing.JLabel;

import edu.stanford.genetics.treeview.core.HeaderFinder;


/**
 *  Internal test class, used only by <code>main</code> test case.
 */
public class TestViewFrame extends ViewFrame {


	TestViewFrame() {
		super("Test Export Panel");
		getContentPane().add(new JLabel("test        test"));
	}


	public void setLoaded(boolean b) { }


	public void update(java.util.Observable obs, java.lang.Object obj) {
	}

	public double noData() {
		return 0.0;
	}

	public UrlPresets getGeneUrlPresets() {
		return null;
	}
	public UrlPresets getArrayUrlPresets() {
		return null;
	}
// hmmm this is kind of an insane dependancy... should get rid of it, methinks.
	public edu.stanford.genetics.treeview.plugin.dendroview.ColorPresets getColorPresets(){return null;}


	public boolean getLoaded() {
		return false;
	}

		public TreeViewApp getApp() {
			return null;
		}

	public DataModel getDataModel() {
		return null;
	}

	public void setDataModel(DataModel model) {
	}

	public HeaderFinder getGeneFinder() {
		return null;
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.ViewFrame#scrollToGene(int)
	 */
	public void scrollToGene(int i) {
		// nothing
		
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.ViewFrame#scrollToArray(int)
	 */
	public void scrollToArray(int i) {
		// nothing
		
	}


	public MainPanel[] getMainPanelsByName(String name) {
		return null;
	}


	public MainPanel[] getMainPanels() {
		return null;
	}
}

