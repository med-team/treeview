/*
 *  BEGIN_HEADER                                              Java TreeView
 *
 *  $Author: alokito $
 *  $RCSfile: DataMatrix.java,v $
 *  $Revision: 1.6 $
 *  $Date: 2005-03-06 01:51:41 $
 *  $Name:  $
 *
 *  This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *  END_HEADER
 */
package edu.stanford.genetics.treeview;

/**
 *  Description of the Interface
 *
 * Provides a simple interface to the actual gene expression data, using the same indexes as the corresponding HeaderInfo objects.
 *
 
 * @author     aloksaldanha
 */
public interface DataMatrix {
	/**
	 *  Gets the value attribute of the DataMatrix object
	 *
	 * @param  row  row (gene) of interest
	 * @param  col  column (array) of interest
	 * @return      The value at the row/col, or possibly some special "missing data" value, as defined by the constants in DataModel.
	 */
	double getValue(int col, int row);

	/**
	 *  Sets the value attribute of an element in the DataMatrix object
	 *
	 * @param  value  value to be set
	 * @param  row  row (gene) of interest
	 * @param  col  column (array) of interest
	 */
	void setValue(double value, int col, int row);

	/**
	 *  Gets the numRow attribute of the DataMatrix object
	 *
	 * @return    The number of rows (genes) in this data matrix.
	 */
	int getNumRow();
	
	/**
	 * Appends a data matrix to the right of this one. Used for comparison of two data sets.
	 * @param m The DataMatrix being appended.
	 */
	
	/**
	 *  Gets the numCol attribute of the DataMatrix object
	 *
	 * @return    The number of columns (arrays) in this data matrix.
	 */
	int getNumCol();
	
	/**
	 *  Gets the numCol attribute of the DataMatrix object before anything was appended to it.
	 *
	 * @return    The number of columns (arrays) in this data matrix before anything was appended.
	 */
	int getNumUnappendedCol();
	
	/**
	 * return true if data matrix has been modified and should be written to file.
	 * @return
	 */
	boolean getModified();

	void setModified(boolean b);
}

