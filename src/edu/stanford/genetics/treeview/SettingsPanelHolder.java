/* BEGIN_HEADER                                              Java TreeView
*
* $Author: alokito $
* $RCSfile: SettingsPanelHolder.java,v $
* $Revision: 1.3 $
* $Date: 2006-03-26 23:24:44 $
* $Name:  $
*
* This file is part of Java TreeView
* Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* END_HEADER
*/
package edu.stanford.genetics.treeview;

import java.awt.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @author aloksaldanha
 *
 * Makes a JPanel with Save and Cancel buttons.
 * The buttons will hide the window if the window is not null.
 */
public class SettingsPanelHolder extends JPanel {

	private Window window = null;
	private ConfigNode configNode = null;
	
	/**
	 * Please use this constructor.
	 * @param w Window to close on Save or Cancel
	 * @param c ConfigNode to store on a save.
	 */
	public SettingsPanelHolder(Window w, ConfigNode c) {
		super();
		window = w;
		configNode = c;
		setLayout(new BorderLayout());
		add(new ButtonPanel(), BorderLayout.SOUTH);
	}
	
	public void synchronizeTo() {
		int n = this.getComponentCount();
		for (int i = 0; i < n; i++) {
			synchronizeTo(i);
		}
	}
	public void synchronizeTo(int i) {
		try {
			((SettingsPanel) getComponent(i)).synchronizeTo();
		} catch (ClassCastException e) {
			// ignore
		}
	}
	
	public void synchronizeFrom() {
		int n = this.getComponentCount();
		for (int i = 0; i < n; i++) {
			synchronizeFrom(i);
		}
	}
	public void synchronizeFrom(int i) {
		try {
			((SettingsPanel) getComponent(i)).synchronizeFrom();
		} catch (ClassCastException e) {
			// ignore
		}
	}
	public void addSettingsPanel(SettingsPanel sP) {
		add((Component) sP, BorderLayout.CENTER);
	}
	  class ButtonPanel extends JPanel {
	  	private void hideWindow() {
	  		if (window == null) {
	  			LogBuffer.println("SettingsPanelHolder.hideWindow(): window is null");
	  		} else {
			  window.setVisible(false);
	  		}
	  	}
		ButtonPanel() {
		  JButton save_button = new JButton("Save");
		  save_button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  synchronizeTo();
			  if (configNode == null) {
			  	LogBuffer.println("SettingsPanelHolder.Save: configNode is null");
			  } else {
			  	configNode.store();
			  }
			  hideWindow();
			}
		  });
		  add(save_button);
		  
		  JButton cancel_button = new JButton("Cancel");
		  cancel_button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  synchronizeFrom();
			  hideWindow();
			}
		  });
		  add(cancel_button);
		}
	  }

}
