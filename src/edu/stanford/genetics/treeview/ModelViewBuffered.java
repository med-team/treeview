/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: ModelViewBuffered.java,v $
 * $Revision: 1.4 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import java.awt.*;
import java.awt.image.MemoryImageSource;

/**
 * superclass, to hold info and code common to all model views
 *
 * This adds buffer management to the modelview.
 * Interestingly, but necessarily, it has no dependancy on any models.
 */
public abstract class ModelViewBuffered extends ModelView {
	protected int []	   offscreenPixels = null;
    protected Image        offscreenBuffer = null;
    protected Graphics     offscreenGraphics = null;

    protected boolean      rotateOffscreen = false;


    protected ModelViewBuffered() {
		super();
    }

    /**
	* this method does no management of instance variables.
	*/
	public Image ensureCapacity(Image i, Dimension req) {
		if (i == null) {
			return createImage(req.width, req.height);
		}
		
		int w = i.getWidth(null);
		int h = i.getHeight(null);
		if ((w < req.width) || (h < req.height)) {
			if (w < req.width) { w = req.width;}
			if (h < req.height) { h = req.height;}
			// should I try to free something?
			Image n = createImage(w, h);
			n.getGraphics().drawImage(i, 0,0,null);
			return n;
		} else {
			return i;
		}
	}

    /**
	* this method sets up all the instance variables.
	* XXX - THIS FAILS ON MAC OS X
	* since mac os x doesn't let you call getGraphics on the Image if it's generated from
	* a pixels array... hmm...
	*/
	protected void ensureCapacity(Dimension req) {
		if (offscreenBuffer == null) {
			createNewBuffer(req.width, req.height);
		}
		
		int w = offscreenBuffer.getWidth(null);
		int h = offscreenBuffer.getHeight(null);
		if ((w < req.width) || (h < req.height)) {
			if (w < req.width) { w = req.width;}
			if (h < req.height) { h = req.height;}
			// should I try to free something?
			createNewBuffer(w,h);
		}
    }

	private synchronized void createNewBuffer(int w, int h) {
		offscreenPixels = new int[w * h];
		MemoryImageSource source = new MemoryImageSource(w, h, offscreenPixels, 0, w);
		source.setAnimated(true);
		Image n = createImage(source);
		if (offscreenBuffer != null) {
			// should I be copying over pixels instead?
			n.getGraphics().drawImage(offscreenBuffer, 0,0,null);
		}
		offscreenBuffer = n;
		offscreenGraphics = offscreenBuffer.getGraphics();
	}
    /* 
     * The double buffer in Swing
     * doesn't seem to be persistent across draws. for instance, every
     * time another window obscures one of ourwindows and then moves,
     * a repaint is triggered by most VMs.
     *
     * We apparently need to maintain our own persistent offscreen
     * buffer for speed reasons...
     */
    public synchronized void paintComponent(Graphics g) {
	Rectangle clip = g.getClipBounds();
//	System.out.println("Entering " + viewName() + " to clip " + clip );

	Dimension newsize = getSize();
	if (newsize == null) { return;}

	Dimension reqSize;
	reqSize = newsize;
	// monitor size changes
	if ((offscreenBuffer == null) ||
	    (reqSize.width != offscreenSize.width) ||
	    (reqSize.height != offscreenSize.height)) {
	    
	    offscreenSize = reqSize;
		offscreenBuffer = ensureCapacity(offscreenBuffer, offscreenSize);
		offscreenGraphics = offscreenBuffer.getGraphics();
	    try {
		((Graphics2D) offscreenGraphics).setRenderingHint(
			RenderingHints.KEY_ANTIALIASING, 
			RenderingHints.VALUE_ANTIALIAS_OFF);
	    } catch (java.lang.NoClassDefFoundError err) {
		// ignore if Graphics2D not found...
	    }
	    offscreenChanged = true;
	} else {
	    offscreenChanged = false;
	}

	// update offscreenBuffer if necessary
	g.setColor(Color.white);
	g.fillRect(clip.x,clip.y,clip.width, clip.height);
	if (isEnabled()) {
	    if ((offscreenSize.width > 0) && (offscreenSize.height > 0)) {
		updateBuffer(offscreenGraphics);
		offscreenValid = true;
	    }
	} else {
//	  	System.out.println(viewName() + " not enabled");
	    Graphics tg= offscreenBuffer.getGraphics();
	    tg.setColor(Color.white);
	    tg.fillRect
		(0, 0, offscreenSize.width, offscreenSize.height);
	}
	
	if (g != offscreenGraphics) { // sometimes paint directly
	    g.drawImage(offscreenBuffer, 0, 0, this);
	}
	paintComposite(g);
//	System.out.println("Exiting " + viewName() + " to clip " + clip );
    }
    
}
