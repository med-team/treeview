/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: ColorPanel.java,v $
 * $Revision: 1.4 $
 * $Date: 2004-12-21 03:28:13 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
/**
 *  encapsulates a panel which can be used to edit a single color within a color
 *  set.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.4 $ $Date: 2004-12-21 03:28:13 $
 */
public class ColorPanel extends JPanel {
	ColorIcon colorIcon;
	int type;
	ColorSetI colorSet;


	/**
	 * @param  i       the type of color panel. This is the type as defined in the <code>ColorSetI</code>, and is how this panel knows what color it is supposed to represent.
	 * @param  colorS  the <code>ColorSetI</code> which this panel represents and modifies.
	 */
	public ColorPanel(int i, ColorSetI colorS) {
		colorSet = colorS;
		type = i;
		redoComps();
	}


	/**  refresh the color of the icon from the <code>ColorSetI</code> */
	public void redoColor() {
		colorIcon.setColor(getColor());
	}


	/**  Remake the UI components */
	public void redoComps() {
		removeAll();
		colorIcon = new ColorIcon(10, 10, getColor());
		JButton pushButton  = new JButton(getLabel(), colorIcon);
		pushButton.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Color trial  = JColorChooser.showDialog(ColorPanel.this, "Pick Color for " + getLabel(), getColor());
					if (trial != null) {
						setColor(trial);
					}
				}
			});

		add(pushButton);
	}


	private void setColor(Color c) {
		colorSet.setColor(type, c);
		colorIcon.setColor(getColor());
		repaint();
	}


	private String getLabel() {
		return colorSet.getType(type);
	}


	private Color getColor() {
		return colorSet.getColor(type);
	}
}


