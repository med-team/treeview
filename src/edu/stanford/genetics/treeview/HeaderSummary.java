/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: HeaderSummary.java,v $
 * $Revision: 1.11 $
 * $Date: 2005-12-05 05:27:53 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;

import java.util.Observable;
/**
* this class generates a single string summary of a HeaderInfo.
*/
public class HeaderSummary extends Observable implements ConfigNodePersistent {
	/**
	 * 
	 */
	public HeaderSummary() {
		super();
	}
	int [] included = new int [] {1};
	
	public void setIncluded(int [] newIncluded) {
		included = newIncluded;
		synchronizeTo();
		setChanged();
		notifyObservers();
	}
	public int [] getIncluded() {
		return included;
	}
	/**
	* returns the best possible summary for the specified index.
	*
	* If no headers are applicable, will return the empty string.
	*/
	public String getSummary(HeaderInfo headerInfo, int index) {
		String [] strings = null;
		try {
		    strings = headerInfo.getHeader(index);
		} catch (java.lang.ArrayIndexOutOfBoundsException aie) {
		    LogBuffer.println("index " + index + " out of bounds on headers, continuing");
			return null;
		}
		if (strings == null) return "";

		StringBuffer out = new StringBuffer();
		int count =0;
		if (included.length == 0) {
			return "";
		}
		for (int i =0; i < included.length; i++) {
			try {
				String test = strings[included[i]];
				if (test != null) {
					if (count != 0) out.append(", ");
					out.append(test);
					count++;
				}
			} catch (java.lang.ArrayIndexOutOfBoundsException aie) {
				// out.append(strings[1]);
			}
		}
		if (count == 0) {
			return "";
		} else {
			return out.toString();
		}
	}
	
	
	public String [] getSummaryArray(HeaderInfo headerInfo, int index) {
		String [] strings = null;
		try {
		    strings = headerInfo.getHeader(index);
		} catch (java.lang.ArrayIndexOutOfBoundsException aie) {
		    LogBuffer.println("index " + index + " out of bounds on headers, continuing");
			return null;
		}
		if (strings == null) return null;

		
		if (included.length == 0) {
			return null;
		}
		String [] out = new String[included.length];
		int count =0;
		for (int i =0; i < included.length; i++) {
			try {
				String test = strings[included[i]];
				out[count] = test;
				count++;
			} catch (java.lang.ArrayIndexOutOfBoundsException aie) {
				// out.append(strings[1]);
			}
		}
		return out;
	}
	private ConfigNode root;
	public void bindConfig(ConfigNode configNode) {
		root  = configNode;
		synchronizeFrom();
	}
	private void synchronizeFrom() {
		if (root == null) return;
		if (root.hasAttribute("included")) {
			String incString = root.getAttribute("included", "1");
			if (incString.equals("")) {
				setIncluded(new int [0]);
			} else {
				int numComma = 0;
				for (int i = 0; i < incString.length(); i++) {
					if (incString.charAt(i) == ',')
						numComma++;
				}
				int [] array = new int[numComma+1];
				numComma = 0;
				int last = 0;
				for (int i = 0; i < incString.length(); i++) {
					if (incString.charAt(i) == ',') {
						Integer x = new Integer(incString.substring(last, i));
						array[numComma++] = x.intValue();
						last = i+1;
					}
				}
				try {
					array[numComma] = (new Integer(incString.substring(last))).intValue();
				} catch (NumberFormatException e) {
					LogBuffer.println("HeaderSummary has trouble restoring included list from "+incString);
				}
				setIncluded(array);
			}
		}
	}
	private void synchronizeTo() {
		if (root == null) return;
		int [] vec = getIncluded();
		StringBuffer temp = new StringBuffer();
		if (vec.length > 0) temp.append(vec[0]);
		for (int i = 1; i < vec.length; i++) {
			temp.append(",");
			temp.append(vec[i]);
		}
		root.setAttribute("included", temp.toString(), "1");
	}
}
