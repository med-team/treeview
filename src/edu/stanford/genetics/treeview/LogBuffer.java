/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: LogBuffer.java,v $
 * $Revision: 1.2 $
 * $Date: 2009-09-04 13:34:37 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import java.util.*;

public class LogBuffer extends Observable {
	private static LogBuffer singleton = new LogBuffer();
	public static void println(String msg) {
		singleton.log(msg);
	}
	public static LogBuffer getSingleton() {
		return singleton;
	}

	private ConfigNode root = new DummyConfigNode("LogBuffer");
	private int defaultLog = 0; // false
	private Vector buffer = new Vector(100, 100);
	public void log(String msg) {
		if (getLog()) {
			append(msg);
		}
		if (getPrint()) {
			System.out.println(msg);
		}
	}
	private boolean getPrint() {
		if (root == null)
			return true;
		else
			return (root.getAttribute("print", 1) == 1);
	}
	/**
	 *
	 * @return true if messages are being logged in the buffer
	 */
	public boolean getLog() {
		return (root.getAttribute("log", defaultLog) == 1);
	}
	
	public void setLog(boolean bool) {
		System.err.println("Before " + getLog() );
		if (bool == getLog())
			return;
		if (bool)
			root.setAttribute("log", 1, defaultLog);
		else
			root.setAttribute("log", 0, defaultLog);
		setChanged();
		notifyObservers(null);
		System.err.println("After " + getLog() );
	}
	
	private void append(String msg) {
		buffer.add(msg);
		setChanged();
		notifyObservers(msg);
	}
	public Enumeration getMessages() {
		return buffer.elements();
	}
	public static void logException(Exception e) {
		println(e.getMessage());
		StackTraceElement [] els =  e.getStackTrace();
		for (int i = 0; i < els.length; i++) {
			StackTraceElement el = els[i];
			println(" - " +el.toString());
		}
	}
}

