/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: Popup.java,v $
 * $Revision: 1.4 $
 * $Date: 2010-05-02 13:33:30 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import java.awt.*;
import java.awt.event.*;
/** This popup was originally designed to do the About message, but now
   it's a generic way to display a dismissable array of strings 
   */

class Popup extends Canvas {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Frame parent; // parent frame associated with popup
    Dialog d; // Holds ref to dialog window
    int line_height;
    int line_widths[];
    int max_width;
    int total_height;
    int margin_width = 10;
    int margin_height = 10;
    
    private String message[];
    private String title;
    private void measure() {
	FontMetrics fm = this.getFontMetrics(this.getFont());
	if (fm == null) return;
	line_height = fm.getHeight();
	max_width = 0;
	for (int i = 0; i < message.length; i++) {
	    line_widths[i] = fm.stringWidth(message[i]);
	    if (line_widths[i] > max_width) {
		max_width = line_widths[i];
	    }
	}
	total_height = message.length * line_height;
    }

    public void center(Rectangle vb) {	
	Dimension d = getSize();
	setLocation( (vb.width - d.width)/2+ vb.x, 
		     (vb.height - d.height)/2 + vb.y);
    }

    public Popup (Frame f, String t, String m[]) {
	title = t;
	message = m;

	d = new Dialog(f, title);
	parent = f;
	d.addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
		    d.dispose();
		}
	});
	line_widths = new int[message.length];
	d.setLayout(new BorderLayout());
	d.add(this, BorderLayout.CENTER);
	
	
	d.add(new DisposePanel(d), BorderLayout.SOUTH);
	
	d.pack();
	Point p = f.getLocation();
	d.setLocation(p.x+10, p.y+10);
	d.setVisible(true);
    }

    public void addNotify() {super.addNotify(); measure();}

    public Dimension getPreferredSize() {
	return new Dimension(max_width + 2*margin_width,
			     message.length*line_height+2*margin_height);
    }

    public void paint(Graphics g) {
	Dimension dim = d.getSize();
	g.setColor(Color.black);
	//	int height =  (dim.height - total_height) / 2;
	int height = line_height;
	for (int i = 0; i < message.length; i++) {
	    g.drawString(message[i], 
			 (dim.width - line_widths[i])/2 , 
			 height);
	    height += line_height;
	}
    }    

/**
 * This class simply displays a close button centered in a panel.
 * Clicking the close button disposes of the window.
 */
class DisposePanel extends Panel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Window m_window;
    public DisposePanel(Window window) {
	m_window = window;
	final Button dispose_button = new Button("Close");
	dispose_button.addActionListener(new ActionListener() {
		// called when close button hit
		public void actionPerformed(ActionEvent evt) {
		    if(evt.getSource() == dispose_button) {
			m_window.dispose();
		    }
		}
	    });
	add(dispose_button);
    }
}

}

