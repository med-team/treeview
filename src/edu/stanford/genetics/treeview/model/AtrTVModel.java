/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: avsegal
 * $RCSfile: AtrTVModel.java
 * $Revision: 
 * $Date: Jun 25, 2004
 * $Name:  
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
 
package edu.stanford.genetics.treeview.model;

/**
 * @author avsegal
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */


import java.awt.Frame;
import java.awt.MenuItem;

import edu.stanford.genetics.treeview.*;


public class AtrTVModel extends TVModel {

	/**
	 * 
	 */
	public AtrTVModel() {
		super();
	}
	public String getType() {
		return "AtrTVModel";
	}	
	
	public double getValue(int x, int y) {
		return -1;
	}
	public void setExprData(double [] newData) {
	}
	ConfigNode documentConfig = new DummyConfigNode("AtrTVModel");
	public ConfigNode getDocumentConfigRoot() {
		return documentConfig;
	}
	
	public void setDocumentConfig(XmlConfig newVal) {
	}
	
	public void setFrame(Frame f) {
	}
	
	public Frame getFrame() {
			return null;
	}
	
	public MenuItem getStatMenuItem() {
		return null;
	}
	
	public void loadNew(FileSet fileSet) throws LoadException {
		resetState();
		setSource(fileSet);
		final AtrTVModelLoader loader = new AtrTVModelLoader(this);
		loader.loadInto(); 
	}
	
}