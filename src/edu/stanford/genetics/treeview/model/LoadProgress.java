/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: LoadProgress.java,v $
 * $Revision: 1.7 $
 * $Date: 2008-06-11 01:58:58 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.model;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * A simple progress dialog. It has a progress bar, a text area which lines can be added to, 
 * and a cancel button with customizable text.
 * 
 * @author aloksaldanha
 *
 */
public class LoadProgress extends JDialog {
    private JProgressBar progressBar;
    private JTextArea taskOutput;
    private String newline = "\n";
	private JButton closeButton;
	public void clear(){
		taskOutput.setText("");
	}
    public void println(String s) {
	taskOutput.append(s + newline);
	taskOutput.setCaretPosition
	    (taskOutput.getDocument().getLength());
    }
	
	public void setButtonText(String text) {
	  closeButton.setText(text);
	}
    public void setLength(int i) {
		setIndeterminate(false);
		if (progressBar.getMaximum() != i) {
			progressBar.setMinimum(0);
			progressBar.setMaximum(i);
		}
    }
    public void setValue(int i) {
	progressBar.setValue(i);
    }
    public void setIndeterminate(boolean flag) {
	// actually, this only works in jdk 1.4 and up...
		// progressBar.setIndeterminate(flag);
    }

    public LoadProgress(String title, Frame f) {
	super(f, title, true);
	progressBar = new JProgressBar();
	progressBar.setValue(0);
	progressBar.setStringPainted(true);
	
	taskOutput = new JTextArea(10, 40);
	taskOutput.setMargin(new Insets(5,5,5,5));
	taskOutput.setEditable(false);
	

	JPanel panel = new JPanel();
	panel.add(progressBar);
    
	JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(panel, BorderLayout.NORTH);
        contentPane.add(new JScrollPane(taskOutput), BorderLayout.CENTER);
	closeButton = new JButton("Cancel");
	closeButton.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			setCanceled(true);
		    LoadProgress.this.dispose();
		}
	    });
	panel = new JPanel();
	panel.add(closeButton);
	contentPane.add(panel, BorderLayout.SOUTH);

        contentPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        setContentPane(contentPane);
    }
	boolean canceled;
	/** Setter for canceled */
	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
	/** Getter for canceled */
	public boolean getCanceled() {
		return canceled;
	}
}
