/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: NatField.java,v $
 * $Revision: 1.6 $
 * $Date: 2004-12-21 03:28:13 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


// NatField: custom Java component: text field that constrains input
// to be numeric.
// Copyright (C) Lemma 1 Ltd. 1997
// Author: Rob Arthan; rda@lemma-one.com

// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// The GNU General Public License can be obtained at URL:
// http://www.lemma-one.com/scrapbook/gpl.html
// or by writing to the Free Software Foundation, Inc., 59 Temple Place,
// Suite 330, Boston, MA 02111-1307 USA.

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

public class NatField extends JTextField implements KeyListener {

	private int val;

	private int max = -1;

	public NatField(int num, int cols) {
		super(Integer.toString(num < 0 ? 0 : num), cols);
		val = num < 0 ? 0 : num;
		addKeyListener(this);
	}

	public NatField(int num, int cols, int maximum) {
		super(Integer.toString(num < 0 ? 0 : num), cols);
		val = num < 0 ? 0 : num;
		max = maximum;
		addKeyListener(this);
	}
	public void keyPressed (KeyEvent evt) {
	}
	public void keyReleased (KeyEvent evt) {
	}

	public void keyTyped(KeyEvent evt) {
		boolean revert;
		int new_val = 10;
		try {
			new_val = Integer.parseInt(getText());
			revert = false;

			if(new_val < 0) { // revert if negative
			  revert = true;
			}
			if(max > 0 && new_val > max) { // revert to max if too big
			  val = max;
			  revert = true;
			}
		} catch (NumberFormatException e) {
		  int len = getText().length();
		  if (len != 0)
			revert = true; // revert if can't convert;
		  else {
			revert = false;
			val = 0;
		  }
		  }
		if(revert) {
//		  System.out.println("Reverting value..");
			setText(Integer.toString(val));
		} else {
//		  System.out.println("keeping value.." + new_val);
			val = new_val;
		}
	}

	public int getNat() {
	  keyTyped(null);
	  return val;
	}
}
