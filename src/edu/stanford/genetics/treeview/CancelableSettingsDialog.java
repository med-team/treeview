/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: CancelableSettingsDialog.java,v $
 * $Revision: 1.6 $
 * $Date: 2004-12-21 03:28:13 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
/**
 *  this is a dialog which displays a single cancelable settings panel
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.6 $ $Date: 2004-12-21 03:28:13 $
 */
public class CancelableSettingsDialog extends JDialog {
	SettingsPanel settingsPanel;
	JDialog settingsFrame;


	/**
	 *
	 * @param  frame  <code>JFrame</code> to block on
	 * @param  title  a title for the dialog
	 * @param  panel  A <code>SettingsPanel</code> to feature in the dialog.
	 */
	public CancelableSettingsDialog(JFrame frame, String title, SettingsPanel panel) {
		super(frame, title, true);
		settingsPanel = panel;
		settingsFrame = this;
		JPanel inner  = new JPanel();
		inner.setLayout(new BorderLayout());
		inner.add((Component) panel, BorderLayout.CENTER);
		inner.add(new ButtonPanel(), BorderLayout.SOUTH);
		getContentPane().add(inner);
		pack();
	}


	class ButtonPanel extends JPanel {
		ButtonPanel() {
			JButton save_button    = new JButton("Save");
			save_button.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							settingsPanel.synchronizeTo();
							settingsFrame.dispose();
						} catch (java.lang.OutOfMemoryError ex) {
							JPanel temp = new JPanel();
							temp. add(new JLabel("Out of memory, try smaller pixel settings or allocate more RAM"));
							temp. add(new JLabel("see Chapter 3 of Help->Documentation... for Out of Memory and Image Export)"));
							
							JOptionPane.showMessageDialog(CancelableSettingsDialog.this,  temp);
						}
					}
				});
			add(save_button);

			JButton cancel_button  = new JButton("Cancel");
			cancel_button.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						settingsPanel.synchronizeFrom();
						settingsFrame.dispose();
					}
				});
			add(cancel_button);
		}
	}
}

