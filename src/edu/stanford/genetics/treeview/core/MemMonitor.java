/* BEGIN_HEADER                                              Java TreeView
*
* $Author: alokito $
* $RCSfile: MemMonitor.java,v $
* $Revision: 1.2 $
* $Date: 2010-05-02 13:34:53 $
* $Name:  $
*
* This file is part of Java TreeView
* Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* END_HEADER 
*/
package edu.stanford.genetics.treeview.core;


import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MemMonitor extends Frame
{
	public String textString;
	Runtime rt;
	
	long freeMem, freeMem2, totalMem, totalMem2;
	
	public static void main(String args[])
	{
		MemMonitor m = new MemMonitor();
		m.start();
	}
	
	public MemMonitor()
	{
		super("VM Memory Example");
		Button clearMem = new Button("Run Garbage Collector");
		add("South", clearMem);
		final MemMonitor top = this;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
		    top.dispose();
			}
		});
	}
	
	public void start()
	{
		setSize(400,250);
		rt = Runtime.getRuntime();
		setVisible(true);
	}
	
	public void paint(Graphics g)
	{
		g.drawString("Free memory (pre-GC)  = " + Long.toString(freeMem), 
		15, 40);
		g.drawString("Used memory (pre-GC)  = " + Long.toString(totalMem-freeMem), 
		15, 55);
		
		g.drawString("Total memory (pre-GC) = " + Long.toString(totalMem), 
		15, 70);
		
		g.drawString("Used memory (post-GC) = " + Long.toString(totalMem2-freeMem2), 
		15, 90);
		g.drawString("Free memory (post-GC) = " + Long.toString(freeMem2), 
		15, 105);
		
		g.drawString("Total memory (post-GC) =" + Long.toString(totalMem2), 
		15, 120);
		
		g.setColor(Color.blue);
		g.drawString("All memory in bytes", 15, 135);
	}
	
	public boolean handleEvent(Event e)
	{
		if(e.target instanceof Button)
		{
			String label = ((Button)e.target).getLabel();
			if(label.equals("Run Garbage Collector"))
			{
				//System.gc();
				freeMem = rt.freeMemory();
				totalMem = rt.totalMemory();
				rt.gc();
				freeMem2 = rt.freeMemory();
				totalMem2 = rt.totalMemory();
				repaint();
				return true;
			}
		}
		return false;
	}
}
