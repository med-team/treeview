/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: Debug.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-18 06:50:18 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.core;

import edu.stanford.genetics.treeview.RCSVersion;

public class Debug {
	/*
	 * (0 < level < 5)
	 * 0 - Debug off
	 * 1 - Programmer Statements
	 * 2 -
	 * 3 -
	 * 4 -
	 * 5 - Everything
	 */
	public static int level = 1;
	static RCSVersion version = 
		new RCSVersion("$Id: Debug.java,v 1.5 2004/12/21 03:28:14 " + 
					   "alokito Exp $, debugging " + (level == 0 ? "off" : "level " + level));

	public static void print(Object caller, String message, Object argument) {
		if (level > 0) {
			String c = (caller == null) ? "" : caller.toString();
			String a = (argument == null) ? "" : argument.toString();
			
			if(c.length() > 100)
				c = c.substring(0, 99);
			if(a.length() > 100)
				a = a.substring(0, 99);
			System.out.println(c + " : " + message + " : " + a);
		}
	}
	public static void print(Object caller, String message) {
		if (level > 0) {
			print(caller, message, (Object) "No Argument");	
		}
	}	
	public static void print(String message, Object argument) {
		if (level > 0) {
			print((Object) "No Caller", message, argument);
		}
	}
	public static void print(String message) {
		if (level > 0) {
			print("No Caller", message, "No Arguments");
		}
	}
	public static void print(Exception e) {
		if (level == 5)
			e.printStackTrace();
	}
}

