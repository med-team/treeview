/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: DummyHeaderInfo.java,v $
 * $Revision: 1.9 $
 * $Date: 2005-11-25 07:24:08 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;

import java.util.Observer;


public class DummyHeaderInfo implements HeaderInfo {
  String [] header1 = new String [] {"Bob1", "Alice1"};
  String [] header2 = new String [] {"Bob2", "Alice2"};
  String [] header3 = new String [] {"Bob3", "Alice3"};
  public String[] getHeader(int i) {
	if (i == 1) {
	  return header1;
	}
	if (i == 2) {
	  return header2;
	}
	  return header3;
  }

	/**
	 *  Gets the header info for gene/array i, col name
	 *
	 * @param  i  index of the header to get
	 * @return    The array of header values
	 */
	public String getHeader(int i, String name) {
	  return (getHeader(i))[getIndex(name)];
	}


	String [] names = new String [] {"Bob", "Alice"};
	/**
	 *  Gets the names of the headers
	 *
	 * @return    The list of names
	 */
	public String[] getNames() {
	  return names;
	}
	public int getNumNames() {
		return names.length;
	}
	public int getNumHeaders() {
	  return 3;
	}

	public int getIndex(String name) {
	  if (name.equals("Bob")) {
		return 0;
	  }
	  return 1;
	}

	public int getHeaderIndex(String id) {
		for (int i = 0; i < getNumHeaders(); i++) {
			if ((getHeader(i)[0]).equals(id)) {
				return i;
			}
		}
		return -1;
	}
	/**
	 * noop, since this object is static.
	 */
	public void addObserver(Observer o) {}		
	public void deleteObserver(Observer o) {}
	public boolean addName(String name, int location) {return false;}
	public boolean setHeader(int i, String name, String value) {return false;}
	public boolean getModified() {return false;}
	public void setModified(boolean mod) {}

	public String getHeader(int rowIndex, int columnIndex) {
		  return (getHeader(rowIndex))[columnIndex];
	}

	  
}


