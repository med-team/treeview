/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: Dpoint.java,v $
 * $Revision: 1.4 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


/**
 *  Class to represent pair of doubles
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version $Revision: 1.4 $ $Date: 2004-12-21 03:28:14 $
 */
public class Dpoint {
	//point who's location is stored in doubles...
	/**
	 *  actual pair of doubles
	 */
	protected double x, y;


	/**
	 *  Constructor for the Dpoint object
	 *
	 * @param  dx  first val in pair
	 * @param  dy  second val in pair
	 */
	public Dpoint(double dx, double dy) {
		x = dx;
		y = dy;
	}


	/**
	 *  Gets the x attribute of the Dpoint object
	 *
	 * @return    The x value
	 */
	public double getX() {
		return x;
	}


	/**
	 *  Gets the y attribute of the Dpoint object
	 *
	 * @return    The y value
	 */
	public double getY() {
		return y;
	}


	/**
	 *  Sometimes we want to scale the x and take the int part
	 *
	 * @param  s  multiplicative scaling factor
	 * @return    int part of product
	 */
	public int scaledX(double s) {
		return (int) (x * s);
	}


	/**
	 *  Sometimes we want to scale the y and take the int part
	 *
	 * @param  s  multiplicative scaling factor
	 * @return    int part of product
	 */
	public int scaledY(double s) {
		return (int) (y * s);
	}


	/**
	 *  Gets the dist to another point
	 *
	 * @param  dp  the other point
	 * @return     The distance to dp
	 */
	public double getDist(Dpoint dp) {
	double dx  = x - dp.getX();
	double dy  = y - dp.getY();
		return dx * dx + dy * dy;
	}
}

