/* BEGIN_HEADER                                              Java TreeView
*
* $Author: alokito $
* $RCSfile: Registration.java,v $
* $Revision: 1.6 $
* $Date: 2006-09-25 22:02:02 $
* $Name:  $
*
* Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* END_HEADER
*/
package edu.stanford.genetics.treeview.reg;


import edu.stanford.genetics.treeview.*;

/**
 * @author aloksaldanha
 *
 * This class keeps track of the registration information for Java Treeview
 * It should be bound to the Registration config node of the global xml config file.
 * 
 */
public class Registration implements ConfigNodePersistent {
	private ConfigNode configNode  = null;
	
	/**
	 * @param node
	 */
	public Registration(ConfigNode node) {
		bindConfig(node);
	}

	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.ConfigNodePersistent#bindConfig(edu.stanford.genetics.treeview.ConfigNode)
	 */
	public void bindConfig(ConfigNode configNode) {
		this.configNode = configNode;
	}

	/**
	 * @param versionTag
	 * @return returns entry corresponding to version tag. Returns null if no corresponding Entry.
	 */
	public Entry getEntry(String versionTag) {
		for (int i = 0; i < getNumEntries(); i++) {
			Entry entry = getEntry(i);
			if (entry.getVersionTag().equals(versionTag))
				return entry;
		}
		return null;
	}

	/**
	 * @param versionTag
	 * @return creates entry corresponding to version tag.
	 */
	public Entry createEntry(String versionTag) {
		Entry oldEntry = getLastEntry();
		ConfigNode newNode = configNode.create("Entry");
		Entry newEntry = new Entry(newNode);
		newEntry.initialize(oldEntry);
		return newEntry;
	}

	/**
	 * @return Number of entries in Registration confignode.
	 */
	public int getNumEntries() {
		ConfigNode [] entries = configNode.fetch("Entry");
		return entries.length;
	}

	/**
	 * @param i
	 * @return i'th entry in ConfigNode.
	 */
	public Entry getEntry(int i) {
		ConfigNode [] entries = configNode.fetch("Entry");
		return new Entry(entries[i]);
	}

	/**
	 * @return current entry
	 */
	public Entry getCurrentEntry() {
		String versionTag = TreeViewApp.getVersionTag();
		return getEntry(versionTag);
	}

	/**
	 * @return
	 */
	public Entry getLastEntry() {
		int numEntries = getNumEntries();
		if (numEntries > 0 ) {
			return getEntry(numEntries-1);
		} else {
			return null;
		}
	}
}
