/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: AppletViewFrame.java,v $
 * $Revision: 1.10 $
 * $Date: 2006-10-03 06:19:12 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview.applet;

import java.applet.Applet;
import java.applet.AppletContext;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Observer;

import javax.swing.JFrame;

import edu.stanford.genetics.treeview.*;
import edu.stanford.genetics.treeview.core.MenuHelpPluginsFrame;

/**
 *  A subclass of ViewFrame designed to run in a browser. Very similar to TreeViewFrame.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.10 $ $Date: 2006-10-03 06:19:12 $
 */
public class AppletViewFrame extends LinkedViewFrame implements Observer {
	/**  Description of the Field */
	TreeViewApp treeView;

	private static String appName = "Java TreeView (Applet)";
	public String getAppName() {
		return appName;
	}
	/**
	 *  Sets up widgets.
	 *
	 * @param  treeview application which spawned this window.
	 */
	public AppletViewFrame(TreeViewApp treeview, Applet applet) 
		{
		super(treeview, appName);
		browserControl= new AppletBrowserControl(applet);
		
	}
	protected void displayPluginInfo() {
		MenuHelpPluginsFrame frame = new MenuHelpPluginsFrame(
				"Current Plugins", this);
		try {
			URL f_currdir;
			f_currdir = new URL(".");
			frame.setSourceText(f_currdir.getPath() + "/plugins");
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
			frame.setSourceText("Unable to read default plugins directory.");
		}
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}
	/**
	 * subclass for applets
	 */
	class AppletBrowserControl extends BrowserControl {
		private AppletContext appletContext;
		
		public AppletBrowserControl(Applet applet) {
			appletContext = applet.getAppletContext();
		}

		public void displayURL(String url) throws IOException {
			appletContext.showDocument(new URL(url), "_blank");
		}
		
	}
}

