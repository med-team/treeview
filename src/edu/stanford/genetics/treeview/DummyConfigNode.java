/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: DummyConfigNode.java,v $
 * $Revision: 1.9 $
 * $Date: 2005-12-05 05:27:53 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import java.util.*;
// vector

/**
 *  This interface defines a ConfigNode without persistence...
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version $Revision: 1.9 $ $Date: 2005-12-05 05:27:53 $
 */
public class DummyConfigNode implements ConfigNode {
	private List<DummyConfigNode> kids;
	private Map<String, String> attr;
	private String name;
	private static final String NULLVALUE = "Null String";

	/**
	 *  create and return a subnode which has the indicated name
	 *
	 * @param  tname  name of subnode to create
	 * @return        newly created node
	 */
	public ConfigNode create(String tname) {
	DummyConfigNode child  = new DummyConfigNode(tname);
		kids.add(child);
		return child;
	}


	/**
	 *  Constructor for the DummyConfigNode object
	 *
	 * @param  tname  name of the (parentless) node to create
	 */
	public DummyConfigNode(String tname) {
		super();
		name = tname;
		kids = new Vector();
		attr = new Hashtable();
	}


	/**
	 *  fetch all nodes with the name
	 *
	 * @param  byname  type of nodes to search for
	 * @return      array of matching nodes
	 */
	public ConfigNode[] fetch(String byname) {
		if (byname == null) {
			return null;
		}
	int matching      = 0;
		for (int i = 0; i < kids.size(); i++) {
			if (byname.equals(((DummyConfigNode) kids.get(i)).name)) {
				matching++;
			}
		}

	ConfigNode[] ret  = new DummyConfigNode[matching];
		matching = 0;

		for (int i = 0; i < kids.size(); i++) {
			if (byname.equals(((DummyConfigNode) kids.get(i)).name)) {
				ret[matching] = (ConfigNode) kids.get(i);
				matching++;
			}
		}
		return ret;
	}


	/**
	 *  fetch first node by name
	 *
	 * @param  byname  type of node to search for
	 * @return        first matching node
	 */
	public ConfigNode fetchFirst(String byname) {
		for (int i = 0; i < kids.size(); i++) {
			if (byname.equals(((DummyConfigNode) kids.get(i)).name)) {
				return (ConfigNode) kids.get(i);
			}
		}
		return null;
	}


	/**
	 *  remove particular subnode
	 *
	 * @param  configNode  node to remove
	 */
	public void remove(ConfigNode configNode) {
		kids.remove(configNode);
	}


	/**
	 *  remove all subnodes by name
	 *
	 * @param  byname type of node to remove
	 */
	public void removeAll(String byname) {
		for (int i = kids.size() - 1; i >= 0; i--) {
			if (byname.equals(((DummyConfigNode) kids.get(i)).name)) {
				kids.remove(i);
			}
		}
	}


	/**
	 *  set attribute to be last in list
	 *
	 * @param  configNode  The new last value
	 */
	public void setLast(ConfigNode configNode) {
		kids.remove(configNode);
		kids.add((DummyConfigNode)configNode);
	}


	/**
	 *  get double attribute
	 *
	 * @param  string  name of attribude
	 * @param  d       a default value to return
	 * @return         The attribute value
	 */
	public double getAttribute(String string, double d) {
	String o  = attr.get(string);
		if ((o == null) || ( o == NULLVALUE)) {
			return d;
		}
		return Double.parseDouble(o);
	}

	/**
	 * determine if a particular attribute is defined for this node.
	 */
	 public boolean hasAttribute(String string) {
	   String o  = attr.get(string);
	   if (o == null) {
		 return false;
	   } else {
		 return true;
	   }
	 }
	 
	/**
	 *  get int attribute
	 *
	 * @param  string  name of attribue
	 * @param  i       default int value
	 * @return         The attribute value
	 */
	public int getAttribute(String string, int i) {
	String o  = attr.get(string);
		if ((o == null) || (o == NULLVALUE)) {
			return i;
		}
		return  Integer.parseInt(o);
	}


	/**
	 *  get String attribute
	 *
	 * @param  string1  attribute to get
	 * @param  string2  Default value
	 * @return          The attribute value
	 */
	public String getAttribute(String string1, String string2) {
	String o  = attr.get(string1);
		if (o == null) {
			return string2;
		}
		if (o == NULLVALUE) {
			return null;
		}
		return (String) o;
	}


	/**
	 *  set double attribute
	 *
	 * @param  att   name of attribute
	 * @param  val   The new attribute value
	 * @param  dval  The default value
	 */
	public void setAttribute(String att, double val, double dval) {
		attr.put(att, new Double(val).toString());
	}


	/**
	 *  set int attribute
	 *
	 * @param  att   name of attribute
	 * @param  val   The new attribute value
	 * @param  dval  The default value
	 */
	public void setAttribute(String att, int val, int dval) {
		attr.put(att, new Integer(val).toString());
	}


	/**
	 *  set String attribute
	 *
	 * @param  att   name of attribute
	 * @param  val   The new attribute value
	 * @param  dval  The default value
	 */
	public void setAttribute(String att, String val, String dval) {
		if (att == null ) LogBuffer.println("attibute to DummyConfig was null!");
		if (val == null) {
			val = NULLVALUE;
		}
		attr.put(att, val);
	}


	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.ConfigNode#fetchOrCreate(java.lang.String)
	 */
	public ConfigNode fetchOrCreate(String string) {
		ConfigNode cand = fetchFirst(string);
		if (cand == null)
			return create(string);
		else 
			return cand;
	}

	/* (non-Javadoc)
	 * @see edu.stanford.genetics.treeview.ConfigNode#store()
	 */
	public void store() {
		// null op, since dummy.
		// System.err.println("Trying to save dummy config")
	}


	@Override
	public void deepCopyTo(final ConfigNode other) {
		// first the attributes...
		for (Object o :attr.keySet()) {
			String key = (String)o;
			Object val = attr.get(key);
			other.setAttribute(key, (String) val, null);
		}
		// then the children
		for (Object kid : kids) {
			DummyConfigNode kidn = (DummyConfigNode) kid;
			ConfigNode nother = other.create(kidn.name);
			kidn.deepCopyTo(nother);
		}
	}
}

