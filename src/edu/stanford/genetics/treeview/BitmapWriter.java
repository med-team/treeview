/* BEGIN_HEADER                                              Java TreeView
*
* $Author: alokito $
* $RCSfile: BitmapWriter.java,v $
* $Revision: 1.6 $
* $Date: 2005-12-05 05:27:53 $
* $Name:  $
*
* This file is part of Java TreeView
* Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* END_HEADER 
*/
package edu.stanford.genetics.treeview;

import java.awt.image.BufferedImage;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.swing.*;

/**
* The purpose of this class is to collect all the messy special-case code for 
* exporting to bitmap images from java.
*
* Unlike the PpmWriter, it is fairly specific to Java Treeview.
*
* It will consist entirely of static methods
*/
public class BitmapWriter {
	public static final String [] formats = new String [] {"png", "ppm", "jpg"};

	/**
	* write image in the specified format to the output stream, popping up dialogs with specified parent in the event of a problem.
	* 
	*/
	public static boolean writeBitmap(BufferedImage i, String format, OutputStream output, JComponent parent){
			if (formats[0].equals(format)) { // png
				try {
					return writePng(i,output, parent);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(parent, 
					new JTextArea("PNG export had problem " +  e ));
					LogBuffer.println("Exception " + e);
					e.printStackTrace();
					return false;
				}
			} else if (formats[1].equals(format)) { // ppm
				try {
					PpmWriter.writePpm(i,output);
					return true;
				} catch (Exception e) {
					JOptionPane.showMessageDialog(parent, 
					new JTextArea("PPM export had problem " +  e ));
					LogBuffer.println("Exception " + e);
					e.printStackTrace();
					return false;
				}
			} else if (formats[2].equals(format)) { // jpeg
				try {
					return writeJpg(i,output, parent);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(parent, 
					new JTextArea("JPEG export had problem " +  e ));
					LogBuffer.println("Exception " + e);
					e.printStackTrace();
					return false;
				}
			} else {
					JOptionPane.showMessageDialog(parent, 
					new JTextArea("Format " +  format + " not supported." ));
					return false;
			}
	}

	/**
	* return true on success, false on failure.
	*
	* may throw up warning screens or messages using parent.
	*/
	public static boolean writeJpg(BufferedImage i, OutputStream output, JComponent parent) throws java.io.IOException {
		String version  = System.getProperty("java.version");
		if (version.startsWith("1.4.0") || version.startsWith("1.4.1")) {
			JOptionPane.showMessageDialog(parent, new JTextArea("You are using Java Version " + version + "\n which has known issues with JPEG export. \nPlease try PNG format or upgrade to 1.4.2 or later if this export fails."));
		}
		
		try {
			ImageIO.write(i,"jpg",output);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(parent, new JTextArea(
			"Problem Saving JPEG " + e + "\n" + 
			"Jpeg export requires Java Version 1.4.1 or better.\n" + 
			"You are running " + version + "\n"+
			"If problem persists, try PPM format, which should always work"));
			return false;
		}
		return true;
	}
	
	/**
	* return true on success, false on failure.
	*
	* may throw up warning screens or messages using parent.
	*/
	public static boolean writePng(BufferedImage i, OutputStream output, JComponent parent) throws java.io.IOException {
		String version  = System.getProperty("java.version");
		
		try {
			ImageIO.write(i,"png",output);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(parent, new JTextArea(
						"Problem Saving PNG " + e +" \n"  +
						"Png export requires Java Version 1.4.1 or better.\n" + 
						"You are running " + version + "\n" +
						"If problem persists, try PPM format, which should always work"));
			return false;
		}
		return true;
	}
}