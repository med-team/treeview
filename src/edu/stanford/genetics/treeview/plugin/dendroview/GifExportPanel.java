/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: GifExportPanel.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:45 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.dendroview;

import java.awt.*;
import java.io.*;

import com.gurge.amd.*;

import edu.stanford.genetics.treeview.*;

/**
*  Subclass of ExportPanel which outputs a postscript version of a DendroView.
*
*/
public class GifExportPanel extends ExportPanel implements SettingsPanel {
  
  // I wish I could just inherit this...
  public GifExportPanel(HeaderInfo arrayHeaderInfo, HeaderInfo geneHeaderInfo, 
	TreeSelectionI geneSelection, 
	TreeSelectionI arraySelection, 
	InvertedTreeDrawer arrayTreeDrawer, LeftTreeDrawer geneTreeDrawer, ArrayDrawer arrayDrawer, MapContainer arrayMap,MapContainer geneMap) {
	super(arrayHeaderInfo, geneHeaderInfo, 
	geneSelection, 
	arraySelection, 
	arrayTreeDrawer, geneTreeDrawer, 
	arrayDrawer, arrayMap,
	geneMap, false);
  }
  
  public void synchronizeTo() {
	save();
  }
  
  public void synchronizeFrom() {
	// do nothing...
  }
  public void save() {
	  try {
		  OutputStream output = new BufferedOutputStream
		  (new FileOutputStream(getFile()));
		  
		  DendroGifWriter gw = new DendroGifWriter();
		  gw.write(output);
		  
		  output.close();
	  } catch (Exception e) {
		  LogBuffer.println("GIF Export Panel caught exception " + e);
	  }
  }
  /**
  * indicate to superclass that this type does not have bbox
  */
  protected boolean hasBbox() { return false;}
  
  protected String getInitialExtension() {
	return(".gif");
  }

  /**
  *  Inner class which outputs a gif version of Dendroview like things
  *
  *	It is "loosely coupled" in that it only calls protected methods in the ExportPanel superclass.
  */
  
  class DendroGifWriter {
	int extraWidth = getBorderPixels();
	int extraHeight = getBorderPixels();
	
	/**
	* write a gif image corresponding to the export panel preview
	* to the OutputStream output.
	*/
	public void write(OutputStream output) {
	  
	  Rectangle destRect = new Rectangle(0,0,
	  estimateWidth(), 
	  estimateHeight());
	  Image i = createImage(destRect.width + extraWidth, destRect.height + extraHeight);
	  Graphics g = i.getGraphics();
	  g.setColor(Color.white);
	  g.fillRect(0,0,destRect.width+1 + extraWidth,  destRect.height+1+extraHeight);
	  g.setColor(Color.black);
	  g.translate(extraHeight/2, extraWidth/2);
	  drawAll(g, 1.0);
	  try {
		  int pixels[][] = TestQuantize.getPixels(i);
		  // quant
		  int palette[] = Quantize.quantizeImage(pixels, 256);
		  GIFEncoder enc = new GIFEncoder(createImage(TestQuantize.makeImage(palette, pixels)));
		  enc.Write(output);
	  } catch (Exception e) {
		LogBuffer.println("In GifExportPanel.DendroGifWriter() got exception " + e);
	  }
	}
	
	}
}


