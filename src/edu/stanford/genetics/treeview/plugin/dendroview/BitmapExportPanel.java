/* BEGIN_HEADER                                              Java TreeView
*
* $Author: rqluk $
* $RCSfile: BitmapExportPanel.java,v $
* $Revision: 1.1 $
* $Date: 2006-08-16 19:13:45 $
* $Name:  $
*
* This file is part of Java TreeView
* Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* END_HEADER 
*/
package edu.stanford.genetics.treeview.plugin.dendroview;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.swing.*;

import edu.stanford.genetics.treeview.*;
/**
*  Subclass of ExportPanel which outputs a bitmap version of a DendroView
*  Supports JPEG, PNG and PPM
*/
public class BitmapExportPanel extends ExportPanel implements SettingsPanel {
	JComboBox formatPulldown = new JComboBox(BitmapWriter.formats);
	/**
	* Default is no char data.
	*/
	public BitmapExportPanel(HeaderInfo arrayHeaderInfo, HeaderInfo geneHeaderInfo, 
	TreeSelectionI geneSelection, 
	TreeSelectionI arraySelection, 
	InvertedTreeDrawer arrayTreeDrawer, LeftTreeDrawer geneTreeDrawer, ArrayDrawer arrayDrawer, MapContainer arrayMap,MapContainer geneMap) {
		this(arrayHeaderInfo, geneHeaderInfo, 
		geneSelection, 
		arraySelection, 
		arrayTreeDrawer, geneTreeDrawer, 
		arrayDrawer, arrayMap,
		geneMap, false);
	}
	public BitmapExportPanel(HeaderInfo arrayHeaderInfo, HeaderInfo geneHeaderInfo, 
	TreeSelectionI geneSelection, 
	TreeSelectionI arraySelection, 
	InvertedTreeDrawer arrayTreeDrawer, LeftTreeDrawer geneTreeDrawer, ArrayDrawer arrayDrawer, MapContainer arrayMap,MapContainer geneMap, boolean hasChar) {
		super(arrayHeaderInfo, geneHeaderInfo, 
		geneSelection, 
		arraySelection, 
		arrayTreeDrawer, geneTreeDrawer, 
		arrayDrawer, arrayMap,
		geneMap, hasChar);
		JPanel holder = new JPanel();
		final JCheckBox appendExt= new JCheckBox("Append Extension?", true);
		formatPulldown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (appendExt.isSelected()) {
					appendExtension();	
				}
			}
		});
		holder.add(new JLabel("Image Format:"));
		holder.add(formatPulldown);
		holder.add(appendExt);
		add(holder);
	}
	private void appendExtension() {
		String fileName = getFilePath();
		int extIndex = fileName.lastIndexOf('.');
		int dirIndex = fileName.lastIndexOf(File.separatorChar);
		if  (extIndex > dirIndex) {
			setFilePath(fileName.substring(0, extIndex) + "." + formatPulldown.getSelectedItem());
		} else {
			setFilePath(fileName + "." + formatPulldown.getSelectedItem());
		}
	}

	
	public void synchronizeTo() {
		save();
	}
	
	public void synchronizeFrom() {
		// do nothing...
	}
	public void save() {
		try {
			OutputStream output = new BufferedOutputStream
			(new FileOutputStream(getFile()));
			
			int extraWidth = getBorderPixels();
			int extraHeight = getBorderPixels();
			Rectangle destRect = new Rectangle(0,0,
			estimateWidth(), estimateHeight());
			
			BufferedImage i = new BufferedImage(destRect.width + extraWidth, destRect.height + extraHeight,               BufferedImage.TYPE_INT_ARGB);
			Graphics g = i.getGraphics();
			g.setColor(Color.white);
			g.fillRect(0,0,destRect.width+1 + extraWidth,  destRect.height+1+extraHeight);
			g.setColor(Color.black);
			g.translate(extraHeight/2, extraWidth/2);
			drawAll(g, 1.0);
			
			String format = (String) formatPulldown.getSelectedItem();
			@SuppressWarnings("unused") // ignore success, could keep window open on failure if save could indicate success.
			boolean success = BitmapWriter.writeBitmap(i, format, output, this);
			
			output.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, 
			new JTextArea("Dendrogram export had problem " +  e ));
			LogBuffer.println("Exception " + e);
			e.printStackTrace();
		}
	}
	/**
	* indicate to superclass that this type does not have bbox
	*/
	protected boolean hasBbox() { return false;}
	
	protected String getInitialExtension() {
		return(".png");
	}
	
	/**
	*  Inner class which outputs a png version of Dendroview like things
	*
	*	It is "loosely coupled" in that it only calls protected methods in the ExportPanel superclass.
	*/
	
	class DendroPngWriter {
		
		/**
		* write a png image corresponding to the export panel preview
		* to the OutputStream output.
		*/
		public void write(OutputStream output) {
			
		}	
	}
}


