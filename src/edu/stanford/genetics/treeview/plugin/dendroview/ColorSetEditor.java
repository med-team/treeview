/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: ColorSetEditor.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:46 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview.plugin.dendroview;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import edu.stanford.genetics.treeview.*;
/**
 *  This class allows editing of a color set...
 *
 * NOTE: This is superceded by the ConfigColorSet stuff in edu.stanford.genetics.treeview,
 * although this code is still used within the dendroview package.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.1 $ $Date: 2006-08-16 19:13:46 $
 */

public class ColorSetEditor extends JPanel {
	private final static int UP       = 0;
	private final static int ZERO     = 1;
	private final static int DOWN     = 2;
	private final static int MISSING  = 3;
	private ColorSet colorSet;


	/**
	 *  Constructor for the ColorSetEditor object
	 *
	 * @param  colorSet  <code>ColorSet</code> to be edited
	 */
	public ColorSetEditor(ColorSet colorSet) {
		this.colorSet = colorSet;
		add(new ColorPanel(UP));
		add(new ColorPanel(ZERO));
		add(new ColorPanel(DOWN));
		add(new ColorPanel(MISSING));
	}


	/**
	 *  A simple test program
	 *
	 * @param  argv  ignored
	 */
	public final static void main(String[] argv) {
		ColorSet temp       = new ColorSet();
		ColorSetEditor cse  = new ColorSetEditor(temp);
		JFrame frame        = new JFrame("ColorSetEditor Test");
		frame.getContentPane().add(cse);
		frame.pack();
		frame.setVisible(true);
	}


	class ColorPanel extends JPanel {
		ColorIcon colorIcon;
		int type;


		ColorPanel(int i) {
			type = i;
			redoComps();
		}


		public void redoComps() {
			removeAll();
			colorIcon = new ColorIcon(10, 10, getColor());
			JButton pushButton  = new JButton(getLabel(), colorIcon);
			pushButton.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Color trial  = JColorChooser.showDialog(ColorSetEditor.this, "Pick Color for " + getLabel(), getColor());
						if (trial != null) {
							setColor(trial);
						}
					}
				});

			add(pushButton);
		}


		private void setColor(Color c) {
			switch (type) {
							case UP:
								colorSet.setUp(c);
								break;
							case ZERO:
								colorSet.setZero(c);
								break;
							case DOWN:
								colorSet.setDown(c);
								break;
							case MISSING:
								colorSet.setMissing(c);
								break;
			}
			colorIcon.setColor(getColor());
//	  redoComps();
			repaint();
		}


		private String getLabel() {
			switch (type) {
							case UP:
								return "Positive";
							case ZERO:
								return "Zero";
							case DOWN:
								return "Negative";
							case MISSING:
								return "Missing";
			}
			return null;
		}


		private Color getColor() {
			switch (type) {
							case UP:
								return colorSet.getUp();
							case ZERO:
								return colorSet.getZero();
							case DOWN:
								return colorSet.getDown();
							case MISSING:
								return colorSet.getMissing();
			}
			return null;
		}
	}

}

