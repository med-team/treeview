/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: FillMap.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:46 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.dendroview;


/**
 *  maps integers (gene index) to pixels, filling available pixels
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version $Revision: 1.1 $ $Date: 2006-08-16 19:13:46 $
 */
public class FillMap extends IntegerMap {
	/**
	 *  Gets the index for a particular pixel.
	 *
	 * @param  i  the pixel value
	 * @return    The index value
	 */
	public int getIndex(int i) {
	  	if (availablepixels == 0) return 0;
		return i * (maxindex - minindex + 1) / availablepixels + minindex;
	}


	/**
	 *  Gets the pixel for a particular index
	 *
	 * @param  i  The index value
	 * @return    The pixel value
	 */
	public int getPixel(int i) {
		return (i - minindex) * availablepixels / (maxindex - minindex + 1);
	}


	/**
	 * @return    The effective scale for the current FillMap
	 */
	public double getScale() {
		return (double) availablepixels / (maxindex - minindex + 1);
	}


	/**
	 * @return    The number of pixels currently being used
	 */
	public int getUsedPixels() {
		if (minindex == -1) {
			return 0;
		} else {
			return availablepixels;
		}
	}


	/**
	 * @return    The number of indexes currently visible
	 */
	public int getViewableIndexes() {
		return maxindex - minindex + 1;
	}


	/**
	 * @return    A short word desribing this type of map
	 */
	public String type() {
		return "Fill";
	}
}

