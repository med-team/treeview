/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: ColorPresets.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:45 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview.plugin.dendroview;

import edu.stanford.genetics.treeview.*;
/**
 *  This class encapsulates a list of Color presets. This is the class to edit the
 *  default presets in...
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.1 $ $Date: 2006-08-16 19:13:45 $
 */

public class ColorPresets implements ConfigNodePersistent {
	private ConfigNode root;
	private final static int dIndex            = 0;// which preset to use if not by confignode?


	/**
	 *  creates a new ColorPresets object and binds it to the node adds default Presets
	 *  if none are currently set.
	 *
	 * @param  parent  node to bind to
	 */
	public ColorPresets(ConfigNode parent) {
		super();
		bindConfig(parent);
		int nNames  = getPresetNames().length;
		if (nNames == 0) {
			addDefaultPresets();
		}
	}


	/**  Constructor for the ColorPresets object */
	public ColorPresets() {
		super();
		root = new DummyConfigNode("ColorPresets");
	}


	/**
	 *  returns default preset, for use when opening a new file which has no color settings
	 */
	public int getDefaultIndex() {
		return root.getAttribute("default", dIndex);
	}


	/**
	 *  True if there a particular preset which we are to default to.
	 *
	 */
	public boolean isDefaultEnabled() {
		return (getDefaultIndex() != -1);
	}


	/**
	 *  Gets the default <code>ColorSet</code>, according to this preset.
	 */
	public ColorSet getDefaultColorSet() {
		int defaultPreset  = getDefaultIndex();
		try {
			return getColorSet(defaultPreset);
		} catch (Exception e) {
			return getColorSet(0);
		}
	}


	/**
	 *  Sets the default to be the i'th color preset.
	 */
	public void setDefaultIndex(int i) {
		root.setAttribute("default", i, dIndex);
	}


	/**  holds the default color sets, which can be added at any time to the extant set */
	public static ColorSet[] defaultColorSets;

	static {
		defaultColorSets = new ColorSet[2];
		defaultColorSets[0] = new ColorSet("RedGreen", "#FF0000", "#000000", "#00FF00", "#909090", "#FFFFFF");
		defaultColorSets[1] = new ColorSet("YellowBlue",
				"#FEFF00", "#000000", "#1BB7E5", "#909090", "#FFFFFF");
	}


	/**  Adds the default color sets to the current presets */
	public void addDefaultPresets() {
		for (int i = 0; i < defaultColorSets.length; i++) {
			addColorSet(defaultColorSets[i]);
		}
	}


	/**
	 *  returns String [] of preset names for display
	 */
	public String[] getPresetNames() {
		ConfigNode aconfigNode[]  = root.fetch("ColorSet");
		String astring[]          = new String[aconfigNode.length];
		ColorSet temp           = new ColorSet();
		for (int i = 0; i < aconfigNode.length; i++) {
			temp.bindConfig(aconfigNode[i]);
			astring[i] = temp.getName();
		}
		return astring;
	}


	/**
	 *  The current number of available presets.
	 */
	public int getNumPresets() {
		ConfigNode aconfigNode[]  = root.fetch("ColorSet");
		return aconfigNode.length;
	}


	/*inherit description */
	public String toString() {
		ConfigNode aconfigNode[]  = root.fetch("ColorSet");
		ColorSet tmp            = new ColorSet();
		String [] names = getPresetNames();
		String ret = "No Presets";
		if (names.length > 0) {
			ret              = "Default is " + names[getDefaultIndex()] + " index " + 
			getDefaultIndex() + "\n";
		}
		for (int index = 0; index < aconfigNode.length; index++) {
			tmp.bindConfig(aconfigNode[index]);
			ret += tmp.toString() + "\n";
		}
		return ret;
	}


	/**
	 *  returns the color set for the ith preset or null, if any exceptions are thrown.
	 *
	 */
	public ColorSet getColorSet(int index) {
		ConfigNode aconfigNode[]  = root.fetch("ColorSet");
		try {
			ColorSet ret  = new ColorSet();
			ret.bindConfig(aconfigNode[index]);
			return ret;
		} catch (Exception e) {
			return null;
		}
	}


	/**
	 *  returns the color set for this name or null, if name not found in kids
	 */
	public ColorSet getColorSet(String name) {
		ConfigNode aconfigNode[]  = root.fetch("ColorSet");
		ColorSet ret            = new ColorSet();
		for (int i = 0; i < aconfigNode.length; i++) {
			ret.bindConfig(aconfigNode[i]);
			if (name.equals(ret.getName())) {
				return ret;
			}
		}
		return null;
	}


	/**
	 *  constructs and adds a <code>ColorSet</code> with the specified attributes.
	 */
	public void addColorSet(String name, String up, String zero, String down, String missing) {
		ColorSet preset  = new ColorSet();
		preset.bindConfig(root.create("ColorSet"));
		preset.setName(name);
		preset.setUp(up);
		preset.setZero(zero);
		preset.setDown(down);
		preset.setMissing(missing);
	}


	/**
	 *  actually copies state of colorset, does not add the colorset itself but a copy.
	 */
	public void addColorSet(ColorSet set) {
		ColorSet preset  = new ColorSet();
		if (root != null) {
			preset.bindConfig(root.create("ColorSet"));
		}
		preset.copyStateFrom(set);
	}


	/*inherit description */
	public void bindConfig(ConfigNode configNode) {
		root = configNode;
		int nNames  = getPresetNames().length;
		if (nNames == 0) {
			addDefaultPresets();
		}
	}


	/**
	 * Remove color set permanently from presets
	 *
	 * @param  i  index of color set
	 */
	public void removeColorSet(int i) {
		ConfigNode aconfigNode[]  = root.fetch("ColorSet");
		root.remove(aconfigNode[i]);
	}


}

