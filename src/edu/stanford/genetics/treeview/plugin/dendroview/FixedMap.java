/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: FixedMap.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:45 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.dendroview;

import edu.stanford.genetics.treeview.*;

/**
 *  Maps integers (gene index) to pixels using a fixed scale
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version $Revision: 1.1 $ $Date: 2006-08-16 19:13:45 $
 */
public class FixedMap extends IntegerMap {
	private double default_scale;
	private double scale;


	/**
	 *  constructs new FixedMap
	 */
	public FixedMap() {
		default_scale = 10.0;
	}


	/**
	 *  For persistance of scale, bind to a ConfigNode
	 *
	 * @param  configNode  ConfigNode to bind to
	 */
	public void bindConfig(ConfigNode configNode) {
		super.bindConfig(configNode);
		scale = root.getAttribute("scale", default_scale);
	}


	/**
	 *  Gets the index for a particular pixel.
	 *
	 * @param  i  the pixel value
	 * @return    The index value
	 */
	public int getIndex(int i) {
		return (int) ((double) i / scale) + minindex;
	}


	/**
	 *  Gets the pixel for a particular index
	 *
	 * @param  i  The index value
	 * @return    The pixel value
	 */
	public int getPixel(int i) {
		return (int) ((double) (i - minindex) * scale);
	}


	/**
	 * @return    The effective scale for the current FillMap
	 */
	public double getScale() {
		return scale;
	}


	/**
	 * @return    The number of pixels currently being used
	 */
	public int getUsedPixels() {
		if (minindex == -1) {
			return 0;
		}
		int i  = (int) ((double) (maxindex - minindex + 1) * scale);
		int j  = (int) (scale * (int) ((double) availablepixels / scale));
		if (i > j) {
			return j;
		} else {
			return i;
		}
	}


	/**
	 * @return    The number of indexes currently visible
	 */
	public int getViewableIndexes() {
	int i  = (int) ((double) availablepixels / scale);
		return i;
	}


	/**
	 *  Sets the defaultScale attribute of the FixedMap object
	 *
	 * @param  d  The new defaultScale value
	 */
	public void setDefaultScale(double d) {
		default_scale = d;
	}


	/**
	 *  set scaling value
	 *
	 * @param  d  The new scale value
	 */
	public void setScale(double d) {
		scale = d;
		root.setAttribute("scale", scale, default_scale);
	}


	/**
	 * @return    A short word desribing this type of map
	 */
	public String type() {
		return "Fixed";
	}
}

