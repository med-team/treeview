/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: FontSettingsPanel.java,v $
 * $Revision: 1.2 $B
 * $Date: 2008-03-09 21:06:34 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.dendroview;

import edu.stanford.genetics.treeview.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
* This class allows selection of Fonts for a FontSelectable.
*/
public class FontSettingsPanel extends JPanel implements SettingsPanel {
	private FontSelectable client;

	public FontSettingsPanel(FontSelectable fs) {
		client = fs;
		setupWidgets();
		updateExample();
	}
	
	public static void main(String [] argv) {
		HeaderInfo hi = new DummyHeaderInfo();
		UrlExtractor ue = new UrlExtractor(hi);
		
		FontSelectable fs = new TextView(hi, ue);
		fs.setPoints(10);
		FontSettingsPanel e  = new FontSettingsPanel(fs);
		JFrame f = new JFrame("Font Settings Test");
		f.add(e);
		f.addWindowListener(new WindowAdapter (){
			public void windowClosing(WindowEvent we) 
			{System.exit(0);}
		});
		f.pack();
		f.setVisible(true);
    }

	public void synchronizeFrom() {
	  setupWidgets();
	}

	public void synchronizeTo() {
	  //nothing to do...
	}
	


	//
// the allowed font styles
//
	/**
	 *  Description of the Field
	 */
	public final static String[] styles  = {
			"Plain",
			"Italic",
			"Bold",
			"Bold Italic"
			};

	/**
	* turn a style number from class java.awt.Font into a string
	 *
	 * @param  style  style index
	 * @return        string description
	 */
	public final static String decode_style(int style) {
		switch (style) {
						case Font.PLAIN:
							return styles[0];
						case Font.ITALIC:
							return styles[1];
						case Font.BOLD:
							return styles[2];
						default:
							return styles[3];
		}
	}

	/**
	* turn a string into a style number
	 *
	 * @param  style  string description
	 * @return        integer encoded representation
	 */
	public final static int encode_style(String style) {
		return
				style == styles[0] ? Font.PLAIN :
				style == styles[1] ? Font.ITALIC :
				style == styles[2] ? Font.BOLD :
				Font.BOLD + Font.ITALIC;
	}

	/**
	 *  Create a blocking dialog containing this component
	 *
	 * @param  f  frame to block
	 */
	public void showDialog(Frame f, String title) {
		JDialog d = new JDialog(f, title);
		d.setLayout(new BorderLayout());
		d.add(this, BorderLayout.CENTER);
		d.add(new ButtonPanel(d), BorderLayout.SOUTH);
		d.addWindowListener(
			new WindowAdapter() {
				public void windowClosing(WindowEvent we) {
					we.getWindow().dispose();
				}
			});
		d.pack();
		d.setVisible(true);
	}


	  
	private JComboBox font_choice;
	private JComboBox style_choice;
	private NatField size_field;
	private JButton display_button;
	private JLabel exampleField;

	String size_prop, face_prop, style_prop;

	
	
	private void setupFontChoice() {
		font_choice = new JComboBox(FontSelector.fonts);
		font_choice.setSelectedItem(client.getFace());
	}
	private void setupStyleChoice() {
		style_choice = new JComboBox(styles);
		style_choice.setSelectedItem(decode_style(client.getStyle()));
	}
	
	private void synchronizeClient() {
		String string  = (String) font_choice.getSelectedItem();
		int i          = encode_style((String) style_choice.getSelectedItem());
		int size       = size_field.getNat();
		client.setFace(string);
		client.setStyle(i);
		client.setPoints(size);
	}
	/**
	*  Sets up widgets
	*/
	private void setupWidgets() {
		removeAll();
		GridBagLayout gbl  = new GridBagLayout();
		setLayout(gbl);
		GridBagConstraints gbc  = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		setupFontChoice();
		add(font_choice, gbc);
		
		setupStyleChoice();
		gbc.gridx = 1;
		add(style_choice, gbc);
		
		size_field = new NatField(client.getPoints(), 3);
		gbc.gridx = 2;
		add(size_field, gbc);
		
		display_button = new JButton("Set");
		display_button.addActionListener(
		new ActionListener() {
			
			public void actionPerformed(ActionEvent actionEvent) {
				updateExample();
				synchronizeClient();
			}
		});
		gbc.gridx = 3;
		add(display_button, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.BOTH;
		exampleField = new JLabel("Font Example Text", JLabel.CENTER);
		add(exampleField, gbc);
	}
	
	private void updateExample() {
		String string  = (String)font_choice.getSelectedItem();
		int i          = encode_style((String)style_choice.getSelectedItem());
		int size       = size_field.getNat();
		//				System.out.println("Setting size to " + size);
		exampleField.setFont(new Font(string, i, size) );
		exampleField.revalidate();
		exampleField.repaint();
	}
	private class ButtonPanel extends JPanel {
	ButtonPanel(Window w) {
		final Window window = w;
	    JButton save_button = new JButton("Close");
	    save_button.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			window.setVisible(false);
		    }
		});
	    add(save_button);

    }    
	}
}

