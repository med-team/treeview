/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: KnnDendroView.java,v $
 * $Revision: 1.2 $
 * $Date: 2006-09-21 17:18:55 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.dendroview;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JScrollBar;

import edu.stanford.genetics.treeview.*;
import edu.stanford.genetics.treeview.model.KnnModel;

/**
 *  This class encapsulates a dendrogram view, which is the classic Eisen
 *  treeview. It uses a drag grid panel to lay out a bunch of linked
 *  visualizations of the data, a la Eisen. In addition to laying out
 *  components, it also manages the GlobalZoomMap. This is necessary since both
 *  the GTRView (gene tree) and KnnGlobalView need to know where to lay out genes
 *  using the same map.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version $Revision: 1.2 $ $Date: 2006-09-21 17:18:55 $
 */
public class KnnDendroView extends DendroView implements ConfigNodePersistent, MainPanel, Observer  {
	/**
	 *  Constructor for the KnnDendroView object
	 *
	 * @param  tVModel   model this KnnDendroView is to represent
	 * @param  vFrame  parent ViewFrame of KnnDendroView
	 */
	public KnnDendroView(KnnModel tVModel, ViewFrame vFrame) {
		super(tVModel, vFrame);
	}
	public KnnDendroView(DataModel tVModel, ConfigNode root, ViewFrame vFrame) {
		super(tVModel, root, vFrame, "KnnDendrogram");
	}
	/**
	 *  This method should be called only during initial setup of the modelview
	 *
	 *  It sets up the views and binds them all to config nodes.
	 *
	 */
	protected void setupViews() {
		DataModel knnModel = getDataModel();
		hintpanel = new MessagePanel("Usage Hints");
		statuspanel = new MessagePanel("View Status");


		ColorPresets colorPresets = DendrogramFactory.getColorPresets();
		ColorExtractor colorExtractor = new ColorExtractor();
		colorExtractor.setDefaultColorSet(colorPresets.getDefaultColorSet());
		colorExtractor.setMissing(KnnModel.NODATA, KnnModel.EMPTY);

		KnnArrayDrawer kArrayDrawer = new KnnArrayDrawer();
		kArrayDrawer.setColorExtractor(colorExtractor);
		arrayDrawer = kArrayDrawer;
		//XXX shouldn't need to observer, should be immuable?
		((Observable) getDataModel()).addObserver(arrayDrawer);




	globalview = new GlobalView();


	// scrollbars, mostly used by maps
	globalXscrollbar = new JScrollBar(JScrollBar.HORIZONTAL, 0,1,0,1);
	globalYscrollbar = new JScrollBar(JScrollBar.VERTICAL,0,1,0,1);
	zoomXscrollbar = new JScrollBar(JScrollBar.HORIZONTAL, 0,1,0,1);
	zoomYscrollbar = new JScrollBar(JScrollBar.VERTICAL,0,1,0,1);



		 zoomXmap = new MapContainer();
		 zoomXmap.setDefaultScale(12.0);
		 zoomXmap.setScrollbar(zoomXscrollbar);
		 zoomYmap = new MapContainer();
		 zoomYmap.setDefaultScale(12.0);
		 zoomYmap.setScrollbar(zoomYscrollbar);

		 // globalmaps tell globalview, atrview, and gtrview
		 // where to draw each data point.
	// the scrollbars "scroll" by communicating with the maps.
		globalXmap = new MapContainer();
		globalXmap.setDefaultScale(2.0);
		globalXmap.setScrollbar(globalXscrollbar);
		globalYmap = new MapContainer();
		globalYmap.setDefaultScale(2.0);
		globalYmap.setScrollbar(globalYscrollbar);

		globalview.setXMap(globalXmap);
		globalview.setYMap(globalYmap);

		globalview.setZoomYMap(getZoomYmap());
		globalview.setZoomXMap(getZoomXmap());

		arraynameview = new ArrayNameView(getDataModel().getArrayHeaderInfo());

		leftTreeDrawer = new LeftTreeDrawer();
		gtrview = new GTRView();
		gtrview.setMap(globalYmap);
		gtrview.setLeftTreeDrawer(leftTreeDrawer);

		invertedTreeDrawer = new InvertedTreeDrawer();
		atrview = new ATRView();
		atrview.setMap(globalXmap);
		atrview.setInvertedTreeDrawer(invertedTreeDrawer);

		atrzview = new ATRZoomView();
		atrzview.setZoomMap(getZoomXmap());
		atrzview.setInvertedTreeDrawer(invertedTreeDrawer);

		zoomview = new ZoomView();
		zoomview.setYMap(getZoomYmap());
		zoomview.setXMap(getZoomXmap());
		zoomview  .setArrayDrawer(arrayDrawer);
		globalview.setArrayDrawer(arrayDrawer);

		arraynameview.setMapping(getZoomXmap());
		arraynameview.setUrlExtractor(viewFrame.getArrayUrlExtractor());


		textview = new TextViewManager(getDataModel().getGeneHeaderInfo(), viewFrame.getUrlExtractor());
		textview.setMap(getZoomYmap());

		doDoubleLayout();

		// reset persistent popups
		settingsFrame = null;
		settingsPanel = null;

		// color extractor
		colorExtractor.bindConfig(getFirst("ColorExtractor"));

		// set data first to avoid adding auto-genereated contrast to documentConfig.
		kArrayDrawer.setDataMatrix(knnModel.getDataMatrix());
		kArrayDrawer.bindConfig(getFirst("ArrayDrawer"));

		// responsible for adding and removing components...
		bindTrees();
		
		globalXmap.bindConfig(getFirst("GlobalXMap"));
		globalYmap.bindConfig(getFirst("GlobalYMap"));
		getZoomXmap().bindConfig(getFirst("ZoomXMap"));
		getZoomYmap().bindConfig(getFirst("ZoomYMap"));

		textview.bindConfig(getFirst("TextViewParent"));
		arraynameview.bindConfig(getFirst("ArrayNameView"));

		// perhaps I could remember this stuff in the MapContainer...
		DataMatrix dataMatrix = getDataModel().getDataMatrix();
		globalXmap.setIndexRange(0, dataMatrix.getNumCol() - 1);
		globalYmap.setIndexRange(0, dataMatrix.getNumRow() - 1);
		getZoomXmap().setIndexRange(-1, -1);
		getZoomYmap().setIndexRange(-1, -1);

		globalXmap.notifyObservers();
		globalYmap.notifyObservers();
		getZoomXmap().notifyObservers();
		getZoomYmap().notifyObservers();
	}
	
}

