/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: ScatterColorSet.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.scatterview;

import edu.stanford.genetics.treeview.*;

/**
* color set for scatterplot view
*/
class ScatterColorSet extends ConfigColorSet {
	private static final String [] types = new String [] {"Background", "Axis", "Data", "Selected"};

	private static final String [] defaults = new String [] {"#000000","#00FF00","#FFFF00", "#FFFFFF"};
	private static final String defaultName = "ScatterColorSet";
	
	ScatterColorSet(String name, 
	String back, String axis, String data, 
	String sel) {
		this(name);
		setColor(0, decodeColor(back));
		setColor(1, decodeColor(axis));
		setColor(2, decodeColor(data));
		setColor(3, decodeColor(sel));
	}
	ScatterColorSet() {
		this("ScatterColorSet");
	}
	ScatterColorSet(String name) {
		super(defaultName, types, defaults);
		setName(name);
	}
	
	public void save(String file) {
		XmlConfig config = new XmlConfig(file, "ScatterColorSet");
		ConfigNode newNode = config.getNode("ConfigColorSet");
		ScatterColorSet tempSet = new ScatterColorSet();
		tempSet.bindConfig(newNode);
		tempSet.copyStateFrom(this);
		config.store();
	}
	
	public void load(String file) {
		XmlConfig config = new XmlConfig(file, "ScatterColorSet");
		ConfigNode newNode = config.getNode("ConfigColorSet");
		ScatterColorSet tempSet = new ScatterColorSet();
		tempSet.bindConfig(newNode);
		copyStateFrom(tempSet);
	}
}
