/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: AxisInfo.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:48 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview.plugin.scatterview;

import edu.stanford.genetics.treeview.*;
class AxisInfo {
	private ConfigNode configNode;
	/** Setter for configNode */
	public void setConfigNode(ConfigNode configNode) {
		this.configNode = configNode;
		initParameters();
	}
	/** Getter for configNode */
	public ConfigNode getConfigNode() {
		return configNode;
	}
	
	private String title;
	/** Setter for title */
	public void setTitle(String title) {
		this.title = title;
	}
	/** Getter for title */
	public String getTitle() {
		return title;
	}
	
	/**
	* The type of this axis, either x or y.
	*/
	private String defaultType = "No Type";
	public String getType() {
		return configNode.getAttribute("type", defaultType);
	}
	public void setType(String type) {
		configNode.setAttribute("type", type, defaultType);
	}
	private AxisParameter [] axisParameters;
	AxisInfo(ConfigNode config) {
		setConfigNode(config);
	}

	private void initParameters() {
		// initialize parameters
		axisParameters = new AxisParameter[4];
		for (int i = 0; i < axisParameters.length; i++) {
			axisParameters[i] = null;
		}
		// copy over existing
		ConfigNode [] existing = configNode.fetch("AxisParameter");
		for (int i = 0; i < existing.length; i++) {
			AxisParameter temp = new AxisParameter(existing[i]);
			axisParameters[temp.getType()] = temp;
		}
		// fill in blanks...
		for (int i = 0; i < axisParameters.length; i++) {
			if (axisParameters[i] == null) {
				ConfigNode newNode = configNode.create("AxisParameter");
				axisParameters[i] = new AxisParameter(newNode);
				axisParameters[i].setType(i);
			}
		}
	}

	public AxisParameter getAxisParameter(int type) {
		return axisParameters[type];
	}
	
	public void copyStateFrom(AxisInfo other) {
		setType(other.getType());
		for (int i =0; i < axisParameters.length; i++) {
			axisParameters[i].copyStateFrom(other.getAxisParameter(i));
		}
	}
}
