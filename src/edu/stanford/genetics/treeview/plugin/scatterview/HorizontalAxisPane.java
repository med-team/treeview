/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: HorizontalAxisPane.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.scatterview;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * a class that plots a vertical axis given an axis info and a scatter color set.
 */
class HorizontalAxisPane extends JPanel  {
	
	private AxisInfo axisInfo;
	/** Setter for axisInfo */
	public void setAxisInfo(AxisInfo axisInfo) {
		this.axisInfo = axisInfo;
		titleLabel.setText(axisInfo.getTitle());
		System.out.println("setting label text to " + axisInfo.getTitle());
		titleLabel.invalidate();
		titleLabel.revalidate();
	}
	/** Getter for axisInfo */
	public AxisInfo getAxisInfo() {
		return axisInfo;
	}
	private ScatterColorSet colorSet;
	/** Setter for colorSet */
	public void setColorSet(ScatterColorSet colorSet) {
		this.colorSet = colorSet;
	}
	/** Getter for colorSet */
	public ScatterColorSet getColorSet() {
		return colorSet;
	}
	JLabel titleLabel = new JLabel();
	/**
	* You'll want to create this after you set the config node for the scatterPane, since it keeps
	* it's own pointers to the axis info and color set.
	*/
	HorizontalAxisPane(AxisInfo axisInfo, ScatterColorSet colorSet) {
		setAxisInfo(axisInfo);
		setColorSet(colorSet);
		add(titleLabel);
	}
	
	public void paintComponent(Graphics g) {
		titleLabel.setForeground(colorSet.getColor("Axis"));
	    titleLabel.setBackground(colorSet.getColor("Background"));
		Dimension size = getSize();
	    g.setColor(colorSet.getColor("Background"));
	    g.fillRect(0,0,size.width, size.height);
	    g.setColor(colorSet.getColor("Axis"));
		g.drawLine(0, size.height/2, size.width, size.height/2);
	}
}

