/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: AxisParameter.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview.plugin.scatterview;

import edu.stanford.genetics.treeview.*;

class AxisParameter {
	private ConfigNode configNode;
	/** Setter for configNode */
	public void setConfigNode(ConfigNode configNode) {
		this.configNode = configNode;
	}
	/** Getter for configNode */
	public ConfigNode getConfigNode() {
		return configNode;
	}
	
	public static final int MIN   = 0;
	public static final int MAX   = 1;
	public static final int MINOR = 2;
	public static final int MAJOR = 3;

	/**
	* the type of this parameter
	*/
	private int defaultType = 0;
	public int getType() {
		return configNode.getAttribute("type", defaultType);
	}
	public void setType(int type) {
		configNode.setAttribute("type", type, defaultType);
	}

	/**
	* the name of this parameter
	*/
	public String getName() {
		switch(getType()) {
			case MIN:
				return "Min";
			case MAX:
				return "Max";
			case MINOR:
				return "Minor";
			case MAJOR:
				return "Major";
		}
		return "Unknown";
	}

	/**
	* is this parameter enabled?
	*/
	private int defaultEnabled = 0;
	public boolean getEnabled() {
		return (configNode.getAttribute("enabled", defaultEnabled) == 1);
	}
	public void setEnabled(boolean b) {
		int val = (b)?1:0;
		configNode.setAttribute("enabled", val, defaultEnabled);
	}
	
	/**
	* what is the value for the parameter?
	*/
	private double defaultValue = 1.0;
	public double getValue() {
		return configNode.getAttribute("value", defaultValue);
	}
	public void setValue(double value) {
		configNode.setAttribute("value", value, defaultValue);
	}
	
	AxisParameter(ConfigNode config) {
		setConfigNode(config);
		
	}
	public void copyStateFrom(AxisParameter other) {
		setType(other.getType());
		setEnabled(other.getEnabled());
		setValue(other.getValue());
	}
}
