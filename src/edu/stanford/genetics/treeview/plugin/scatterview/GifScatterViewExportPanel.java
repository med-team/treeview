/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: GifScatterViewExportPanel.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.scatterview;

import java.awt.Image;
import java.io.*;

import com.gurge.amd.*;

import edu.stanford.genetics.treeview.LogBuffer;

class GifScatterViewExportPanel extends ScatterViewExportPanel {
	GifScatterViewExportPanel(ScatterView scatterView) {
		super(scatterView);
	}

  public void synchronizeTo() {
	save();
  }
  
  public void synchronizeFrom() {
	// do nothing...
  }
  public void save() {
	  try {
		  OutputStream output = new BufferedOutputStream
		  (new FileOutputStream(getFile()));
		  
		  write(output);
		  
		  output.close();
	  } catch (Exception e) {
		  LogBuffer.println("GIF ColorBar Export Panel caught exception " + e);
	  }
  }
 
	/**
	* Save image to the currently selected file...
	*/
	public void write(OutputStream output) {
		Image i = generateImage();
		try {
			int pixels[][] = TestQuantize.getPixels(i);
			// quant... probably unnecessary here...
			int palette[] = Quantize.quantizeImage(pixels, 256);
			GIFEncoder enc = new GIFEncoder(createImage(TestQuantize.makeImage(palette, pixels)));
			enc.Write(output);
		} catch (Exception e) {
			LogBuffer.println("In GifScatterViewExportPanel.synchronizeTo() got exception " + e);
		}
	}
}


