/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: FileSetEditor.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.karyoview;

import edu.stanford.genetics.treeview.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
/**
* This class allows editing of a file set...
*/

public class FileSetEditor extends JPanel {
  private FileSet fileSet;
  /** Setter for fileSet */
  public void setFileSet(FileSet fileSet) {
	  this.fileSet = fileSet;
  }
  /** Getter for fileSet */
  public FileSet getFileSet() {
	  return fileSet;
  }
 public FileSetEditor(FileSet fileSet, Window jFrame) {
	 setFileSet(fileSet);
	 final JLabel desc = new JLabel(fileSet.toString());
	 add(desc);
	 final Window frame = jFrame;
	  JButton pushButton = new JButton("Find...");
	  pushButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {			
			JFileChooser fileDialog = new JFileChooser();
			CdtFilter ff = new CdtFilter();
			fileDialog.setFileFilter(ff);
			
			String string = getFileSet().getDir();
			if (string != null) {
				fileDialog.setCurrentDirectory(new File(string));
			}
			int retVal = fileDialog.showOpenDialog(frame);
			if (retVal == JFileChooser.APPROVE_OPTION) {
				File chosen = fileDialog.getSelectedFile();
				
				FileSet fileSet1 = new FileSet(chosen.getName(), chosen.getParent()+File.separator);
				fileSet1.setName(getFileSet().getName());
				getFileSet().copyState(fileSet1);
			}
			desc.setText(getFileSet().toString());
			desc.revalidate();
			desc.repaint();
		}
	  });
	  add(pushButton);
  }
  
  public static final void main(String [] argv) {
	FileSet temp = new FileSet(new DummyConfigNode("DummyFileSet"));
	JFrame frame = new JFrame("FileSetEditor Test");
	FileSetEditor cse = new FileSetEditor(temp, frame);
	frame.getContentPane().add(cse);
	frame.pack();
	frame.setVisible(true);
  }
  
}
