/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: ChromosomeLocus.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.karyoview;


/**
* The purpose of this class is purely to hold information about the location of
* various loci. It does not hold any exporession data directly, although
* it hold a unique integer, the cdtIndex, which ought to be used to look up any values
* in a table.
*/
class ChromosomeLocus {
	public static final int LEFT = 1;
	public static final int RIGHT = 2;
	public static final int CIRCULAR = 3;

	private int chromosome; // you should follow organism convention here
	private int arm; //currently, must be linearized genome
	private double position; // distance from centromere. you do the math
	private int cdtIndex; // row index of cdt corresponding to locus.
	private ChromosomeLocus leftLocus = null; // left neightbor or null
	private ChromosomeLocus rightLocus = null; // right neightbor or null
	
	
	// full getters and setters...
	public int  getChromosome() {return chromosome;}
	public int         getArm() {return arm;}
	public double getPosition() {return position;}
	public int    getCdtIndex() {return cdtIndex;}
	public ChromosomeLocus getLeft() {return leftLocus;}
	public ChromosomeLocus getRight() {return rightLocus;}
	
	public void setChromosome(int chr) {this.chromosome = chr;}
	public void        setArm(int arm) {this.arm        = arm;}
	public void   setPosition(double pos) {this.position   = pos;}
	public void   setCdtIndex(int idx) {this.cdtIndex = idx;}
	public void setLeft(ChromosomeLocus left) {leftLocus = left;}
	public void setRight(ChromosomeLocus right) {rightLocus = right;}

	ChromosomeLocus(int chr, int arm, double pos, int index) {
		this.chromosome = chr;
		this.arm = arm;
		this.position = pos;
		this.cdtIndex = index;
	}
	
	public String getText(int arm) {
	  if (arm == LEFT) {
		return "Left";
	  } else {
		return "Right";
	  }
	}
	
	public String toString() {
	  return "Chr " + chromosome + ", arm " + getText(arm) + ", pos " + position + ", row in cdt " + cdtIndex;
	}
	
}
