/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: CircularChromosome.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.karyoview;


class CircularChromosome extends Chromosome {
	ChromosomeLocus [] circularArm;
	CircularChromosome(int nCircular) {
		circularArm = new ChromosomeLocus[nCircular];
	}
	public void insertLocus(ChromosomeLocus locus) {
		if (locus.getArm() == ChromosomeLocus.CIRCULAR) {
			insertLocusIntoArray(circularArm, locus);
		}
	}
	public double getMaxPosition() {
		return circularArm[circularArm.length - 1].getPosition();
	}
	public double getMaxPosition(int arm) {
		if (arm == ChromosomeLocus.CIRCULAR) {
			return getMaxPosition();
		}
		return 0.0;
	}
	public int getType() {
		return Chromosome.CIRCULAR;
	}
	/**
	* returns locus at 0 min
	*/
	public ChromosomeLocus getLeftEnd() {
		return circularArm[0];
	}
	/**
	* returns locus at latest min
	*/
	public ChromosomeLocus getRightEnd() {
		return circularArm[circularArm.length - 1];
	}

	public ChromosomeLocus getClosestLocus(int arm, double position) {
		if (arm == ChromosomeLocus.CIRCULAR) {
			return getLocusRecursive(position, circularArm, 0, circularArm.length-1);
		}
		return null;
	}
	public ChromosomeLocus getLocus(int arm, int index) {
		if (arm == ChromosomeLocus.CIRCULAR) {
			return circularArm[index];
		}
		return null;
	}
}
