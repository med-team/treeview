/* BEGIN_HEADER                                              Java TreeView
*
* $Author: rqluk $
* $RCSfile: NeighborAverager.java,v $
* $Revision: 1.1 $
* $Date: 2006-08-16 19:13:50 $
* $Name:  $
*
* This file is part of Java TreeView
* Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* END_HEADER 
*/
package edu.stanford.genetics.treeview.plugin.karyoview;



  /**
  * average by neighbors.
  * k must be an odd integer. Returns the average of 
  *  the locus,
  *  the (k-1)/2 loci on the left,
  *  and the (k-1)/2 loci on the right.
  */
  class NeighborAverager extends GroupAverager {
	  int defaultNum = 5;
	  /** Setter for num */
	  public void setNum(int num) {
		  if (num % 2 == 0) {
			  num++;
		  }
		  getConfigNode().setAttribute("num", num, defaultNum);
	  }
	  /** Getter for num */
	  public int getNum() {
		  return getConfigNode().getAttribute("num", defaultNum);
	  }

	  public NeighborAverager() {
		  super();
	  }
	  public NeighborAverager(int n) {
		  setNum(n);
	  }


	  public int getType() {
		  return Averager.NEIGHBOR;
	  }
	  public String getPre() {
		  return  getNum() + " Neighbors of ";
	  }
	  public String getArg() {
		  return "" + getNum();
	  }

	  protected ChromosomeLocus [] getContributors(ChromosomeLocus locus) {
		  int num = getNum();
		  ChromosomeLocus [] ret = new ChromosomeLocus[num];
		  ChromosomeLocus leftCand  = locus.getLeft();
		  ChromosomeLocus rightCand = locus.getRight();
		  int onAside = (num -1) /2;
		  int ncand = 0;
		  if (isNodata(locus) == false) {
//			  System.out.println("adding self " + locus);
			  ret[ncand++] = locus;
		  }
		  // populate left side...
		  int nOnLeft = 0;
		  while ((leftCand != null) && (nOnLeft < onAside)) {
			  // make sure leftCand is viable...
			  while ((leftCand != null) && isNodata(leftCand)) {
				  leftCand = leftCand.getLeft();
			  }
			  if (leftCand != null) {
				  ret[ncand++] = leftCand;
				  leftCand = leftCand.getLeft();
				  nOnLeft++;
			  }
		  }
		  
		  // populate right side...
		  int nOnRight = 0;
		  while ((rightCand != null) && (nOnRight < onAside)) {
			  // make sure rightCand is viable...
			  while ((rightCand != null) && isNodata(rightCand)) {
				  rightCand = rightCand.getRight();
			  }
			  if (rightCand != null) {
				  ret[ncand++] = rightCand;
				  rightCand = rightCand.getRight();
				  nOnRight++;
			  }
		  }
		  return ret;
	  }
  }
  
  
