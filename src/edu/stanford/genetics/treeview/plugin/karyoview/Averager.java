/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: Averager.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.karyoview;

import edu.stanford.genetics.treeview.*;

/**
* this base class doesn't actually average. All other methods should subclass.
*/
class Averager {
	public final static int SIMPLE = 0;
	public final static int NEAREST = 1;
	public final static int NEIGHBOR = 2;
	public final static int INTERVAL = 3;
	
	protected KaryoView karyoView;
	/** Setter for karyoView */
	public void setKaryoView(KaryoView karyoView) {
		this.karyoView = karyoView;
	}
	/** Getter for karyoView */
	public KaryoView getKaryoView() {
		return karyoView;
	}
	
	  private ConfigNode configNode = new DummyConfigNode("Averager");
	  /** Setter for configNode */
	  public void bindConfig(ConfigNode configNode) {
		  this.configNode = configNode;
	  }
	  /** Getter for configNode */
	  public ConfigNode getConfigNode() {
		  return configNode;
	  }

	
	
	protected String [] message = new String [2];
	public String[] getDescription(ChromosomeLocus locus, int col) {
		int row = locus.getCdtIndex();
		message[0] = "Gene " + getKaryoView().getGeneInfo().getHeader(row, "YORF");
		message[1] = "No averaging";
		return message;
	}
	public double getValue(ChromosomeLocus locus,int col) {
		int row = locus.getCdtIndex();
		return karyoView.getDataMatrix().getValue(col, row);
	}
	public int getType() {
		return Averager.SIMPLE;
	}
	public String getArg() {
		return null;
	}
}

