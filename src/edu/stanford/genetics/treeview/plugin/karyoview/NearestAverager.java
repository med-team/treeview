/* BEGIN_HEADER                                              Java TreeView
*
* $Author: rqluk $
* $RCSfile: NearestAverager.java,v $
* $Revision: 1.1 $
* $Date: 2006-08-16 19:13:49 $
* $Name:  $
*
* This file is part of Java TreeView
* Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* END_HEADER 
*/
package edu.stanford.genetics.treeview.plugin.karyoview;



    /**
  * average k-nearest neighbors of point. If two points are equally close, an
  * unspecified one will be picked.
  */
  class NearestAverager extends GroupAverager {
	  int defaultNum = 5;
	  /** Setter for num */
	  public void setNum(int num) {
		  getConfigNode().setAttribute("num", num, defaultNum);
	  }
	  /** Getter for num */
	  public int getNum() {
		  return getConfigNode().getAttribute("num", defaultNum);
	  }
	  public NearestAverager() {
		  super();
	  }
	  public NearestAverager(int n) {
		  setNum(n);
	  }
	  public int getType() {
		  return Averager.NEAREST;
	  }
	  public String getArg() {
		  return "" + getNum();
	  }

	  protected String getPre() {
		  return "Nearest " + getNum() + " of ";
	  }
	  protected ChromosomeLocus [] getContributors(ChromosomeLocus locus) {
		  ChromosomeLocus [] ret = new ChromosomeLocus[getNum()];
		  ChromosomeLocus leftCand  = locus.getLeft();
		  ChromosomeLocus rightCand = locus.getRight();
		  int num = getNum();
		  int ncand = 0;
		  if (isNodata(locus) == false) {
//			  System.out.println("adding self " + locus);
			  ret[ncand++] = locus;
		  }
		  // the leftCand != rightCand works for both linear and circular chromosomes,
		  // since with a circle they will wrap around, and with a linear they will both
		  // eventually become null.
		  while ((ncand < num) && (leftCand != rightCand)) {
			  // make sure leftCand is viable...
			  while ((leftCand != null) && isNodata(leftCand)) {
				  leftCand = leftCand.getLeft();
			  }
			  
			  // make sure rightCand is viable...
			  while ((rightCand != null) && isNodata(rightCand)) {
				  rightCand = rightCand.getRight();
			  }
			  
			  if ((leftCand == null) && (rightCand != null)) {
				  ret[ncand++] = rightCand;
				  rightCand = rightCand.getRight();
			  }
			  
			  if ((leftCand != null) && (rightCand == null)) {
				  ret[ncand++] = leftCand;
				  leftCand = leftCand.getLeft();
			  }
			  
			  if ((leftCand != null) && (rightCand != null)) {
				  double leftDist =  getDist(leftCand, locus);
				  double rightDist = getDist(rightCand, locus);
				  if (leftDist < rightDist) {
//					  System.out.println("adding left " + leftCand);
					  ret[ncand++] = leftCand;
					  leftCand = leftCand.getLeft();
					  if (leftCand != null) leftDist =  getDist(leftCand, locus);
				  } else {
//					  System.out.println("adding right " + rightCand);
					  ret[ncand++] = rightCand;
					  rightCand = rightCand.getRight();
					  if (rightCand != null) rightDist = getDist(rightCand, locus);
				  }
			  }
		  }
		  return ret;
	  }
	  
  }
  
