/* BEGIN_HEADER                                              Java TreeView
*
* $Author: rqluk $
* $RCSfile: IntervalAverager.java,v $
* $Revision: 1.1 $
* $Date: 2006-08-16 19:13:49 $
* $Name:  $
*
* This file is part of Java TreeView
* Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* END_HEADER 
*/
package edu.stanford.genetics.treeview.plugin.karyoview;



/**
* average all points which are closer than width/2 units away.
*/
class IntervalAverager extends GroupAverager {
	double defaultWidth = 1.0;
	/** Setter for width */
	public void setWidth(double width) {
		getConfigNode().setAttribute("width", width, defaultWidth);
	}
	/** Getter for width */
	public double getWidth() {
		return getConfigNode().getAttribute("width", defaultWidth);
	}
	
	public IntervalAverager() {
		super();
	}
	public IntervalAverager(double width) {
		super();
		setWidth(width);
	}
	
	
	public int getType() {
		return Averager.INTERVAL;
	}
	public String getArg() {
		return "" + getWidth();
	}
	
	protected String getPre() {
		return  "Interval size " + getWidth() + " units around ";
	}
	
	protected ChromosomeLocus [] getContributors(ChromosomeLocus locus) {
		ChromosomeLocus leftCand  = locus.getLeft();
		ChromosomeLocus rightCand = locus.getRight();
		int totalCand = 0;
		// first, calculate number of candidates...
		if (isNodata(locus) == false) {
			totalCand++;
		}
		double radius = getWidth()/2;
		while ((leftCand != null) && (getDist(leftCand, locus) < radius)) {
			if (isNodata(leftCand) == false) {
				totalCand++;
			}
			leftCand = leftCand.getLeft();
		}
		while ((rightCand != null) && (getDist(rightCand, locus) < radius)) {
			if (isNodata(rightCand) == false) {
				totalCand++;
			}
			rightCand = rightCand.getRight();
		}
		
		
		//okay, let's allocate...
		ChromosomeLocus [] ret = new ChromosomeLocus[totalCand];
		leftCand  = locus.getLeft();
		rightCand = locus.getRight();
		totalCand = 0;
		
		// populate...
		if (isNodata(locus) == false) {
			ret[totalCand++] = locus;
		}
		while ((leftCand != null) && (getDist(leftCand, locus) < radius)) {
			if (isNodata(leftCand) == false) {
				ret[totalCand++] = leftCand;
			}
			leftCand = leftCand.getLeft();
		}
		while ((rightCand != null) && (getDist(rightCand, locus) < radius)) {
			if (isNodata(rightCand) == false) {
				ret[totalCand++] = rightCand;
			}
			rightCand = rightCand.getRight();
		}
		return ret;
	}
}


