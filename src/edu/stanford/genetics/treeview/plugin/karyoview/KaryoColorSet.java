/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: KaryoColorSet.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.karyoview;

import edu.stanford.genetics.treeview.*;

/**
* color set for karyoscope view
*/
class KaryoColorSet extends ConfigColorSet {
	private static final String [] types = new String [] {"Up", "Down", "Highlight", "Genome", "Background", "Line"};

	private static final String [] defaults = new String [] {"#FF0000","#00FF00","#FFFF00", "#FFFFFF", "#000000", "#A0A0A0"};
	private static final String defaultName = "KaryoColorSet";
	
	KaryoColorSet(String name, 
	String up, String down, String high, 
	String genome, String back, String line) {
		this(name);
		setColor(0, decodeColor(up));
		setColor(1, decodeColor(down));
		setColor(2, decodeColor(high));
		setColor(3, decodeColor(genome));
		setColor(4, decodeColor(back));
		setColor(5, decodeColor(line));
	}
	KaryoColorSet() {
		this("KaryoColorSet");
	}
	KaryoColorSet(String name) {
		super(defaultName, types, defaults);
		setName(name);
	}
	
	public void save(String file) {
		XmlConfig config = new XmlConfig(file, "KaryoColorSet");
		ConfigNode newNode = config.getNode("ConfigColorSet");
		KaryoColorSet tempSet = new KaryoColorSet();
		tempSet.bindConfig(newNode);
		tempSet.copyStateFrom(this);
		config.store();
	}
	
	public void load(String file) {
		XmlConfig config = new XmlConfig(file, "KaryoColorSet");
		ConfigNode newNode = config.getNode("ConfigColorSet");
		KaryoColorSet tempSet = new KaryoColorSet();
		tempSet.bindConfig(newNode);
		copyStateFrom(tempSet);
	}
}
