/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: rqluk $
 * $RCSfile: LinearChromosome.java,v $
 * $Revision: 1.1 $
 * $Date: 2006-08-16 19:13:49 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview.plugin.karyoview;


class LinearChromosome extends Chromosome {
	ChromosomeLocus [] leftArm;
	ChromosomeLocus [] rightArm;
	LinearChromosome(int nLeft, int nRight) {
//		System.out.println("Linear chromosome (" + nLeft + ", " + nRight + ")");
		leftArm = new ChromosomeLocus[nLeft];
		rightArm = new ChromosomeLocus[nRight];
	}
	public void insertLocus(ChromosomeLocus locus) {
		ChromosomeLocus rightLocus = null;
		ChromosomeLocus leftLocus = null;
		if (locus.getArm() == ChromosomeLocus.LEFT) {
			int point = insertLocusIntoArray(leftArm, locus);
			if (point == -1) {
				System.out.println("could not fit locus on right arm which has length " + rightArm.length);
			} else {
				// find right...
				if (point == 0) {
					rightLocus = (rightArm.length == 0)? null: rightArm[0];
				} else {
					rightLocus = leftArm[point-1];
				}
				
				//find left ...
				if (point == leftArm.length-1) {
					leftLocus = null;
				} else {
					leftLocus = leftArm[point+1];
				}
			}
		} else if (locus.getArm() == ChromosomeLocus.RIGHT) {
			int point = insertLocusIntoArray(rightArm, locus);
			if (point == -1) {
				System.out.println("could not fit locus on right arm which has length " + rightArm.length + " which contains ");
				for (int i = 0; i < rightArm.length; i++) {
					System.out.println(rightArm[i].toString());
				}
				
			} else {
				// find left...
				if (point == 0) {
					leftLocus = (leftArm.length == 0)? null : leftArm[0];
				} else {
					leftLocus = rightArm[point-1];
				}
				
				//find right ...
				if (point == rightArm.length-1) {
					rightLocus = null;
				} else {
					rightLocus = rightArm[point+1];
				}
			}
		}
		locus.setLeft(leftLocus);
		locus.setRight(rightLocus);
		if (leftLocus != null) leftLocus.setRight(locus);
		if (rightLocus != null) rightLocus.setLeft(locus);

	}
	public double getMaxPosition() {
		double leftMax = (leftArm.length == 0)? 0: 
			leftArm[leftArm.length - 1].getPosition();
		double rightMax = (rightArm.length == 0)? 0: 
			rightArm[rightArm.length - 1].getPosition();
		return (leftMax > rightMax) ? leftMax : rightMax;
	}
	public double getMaxPosition(int arm) {
		ChromosomeLocus end = null;
		if (arm == ChromosomeLocus.LEFT) {
			end = getLeftEnd();
		} else if (arm == ChromosomeLocus.RIGHT) {
			end = getRightEnd();
		}
		return (end == null) ? 0.0 : end.getPosition();
	}
	public ChromosomeLocus getClosestLocus(int arm, double position) {
		if (arm == ChromosomeLocus.LEFT) {
			return getLocusRecursive(position, leftArm, 0, leftArm.length-1);
		} else if (arm == ChromosomeLocus.RIGHT) {
			return getLocusRecursive(position, rightArm, 0, rightArm.length-1);
		}
		return null;
	}
	public int getType() {
		return Chromosome.LINEAR;
	}
	public ChromosomeLocus getLeftEnd() {
		if (leftArm.length != 0) {
			return leftArm[leftArm.length - 1];
		}
		if (rightArm.length != 0) {
			return rightArm[0];
		}
		return null;
	}
	public ChromosomeLocus getRightEnd() {
		if (rightArm.length != 0) {
			return rightArm[rightArm.length - 1];
		}
		if (leftArm.length != 0) {
			return leftArm[0];
		}
		return null;
	}
	public ChromosomeLocus getLocus(int arm, int index) {
		if (arm == ChromosomeLocus.LEFT) {
			return leftArm[index];
		} else if (arm == ChromosomeLocus.RIGHT) {
			return rightArm[index];
		}
		return null;
	}
}
