/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: CoordinatesSettingsPanel.java,v $
 * $Revision: 1.2 $
 * $Date: 2008-03-09 21:06:34 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview.plugin.karyoview;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import edu.stanford.genetics.treeview.*;

class CoordinatesSettingsPanel extends JPanel implements SettingsPanel {
	private KaryoPanel karyoPanel;
	/** Setter for karyoPanel */
	public void setKaryoPanel(KaryoPanel karyoPanel) {
		this.karyoPanel = karyoPanel;
	}
	/** Getter for karyoPanel */
	public KaryoPanel getKaryoPanel() {
		return karyoPanel;
	}
	
	private CoordinatesPresets coordinatesPresets;
	/** Setter for coordinatesPresets */
	public void setCoordinatesPresets(CoordinatesPresets coordinatesPresets) {
		this.coordinatesPresets = coordinatesPresets;
	}
	/** Getter for coordinatesPresets */
	public CoordinatesPresets getCoordinatesPresets() {
		return coordinatesPresets;
	}

	public CoordinatesSettingsPanel(KaryoPanel karyoPanel, CoordinatesPresets coordsPresets, ViewFrame frame) {
		setKaryoPanel(karyoPanel);
		setCoordinatesPresets(coordsPresets);
		setFrame(frame);
		configureWidgets();
		addWidgets();
	}
	public void setEnabled(boolean enabled) {
		fileButton.setEnabled(enabled);
		originalButton.setEnabled(enabled);
		for (int i =0; i < presetButtons.length; i++) {
			presetButtons[i].setEnabled(enabled);
		}
	}
	private JButton fileButton, originalButton;
	private JButton [] presetButtons;
	private ViewFrame frame  = null;
	/** Setter for frame */
	public void setFrame(ViewFrame frame) {
		this.frame = frame;
	}
	/** Getter for frame */
	public ViewFrame getFrame() {
		return frame;
	}
	private void addWidgets() {
		this.removeAll();
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		gc.weightx = 100;
		gc.weighty = 100;
		gc.gridx = 0;
		gc.gridy = 0;
		gc.gridwidth = 1;
		gc.gridheight = 1;

		add(originalButton, gc);
		
		JPanel presetPanel = new JPanel();
		for (int i =0; i < presetButtons.length;i++) {
			presetPanel.add(presetButtons[i]);
		}
		presetPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		gc.gridy = 1;
		//		  add(new JScrollPane(presetPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), gbc);

		add(presetPanel, gc);
		gc.gridy = 2;
		add(fileButton, gc);

	}
	
	private void configureWidgets() {
		originalButton = new JButton("Extract from Cdt");
		originalButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				karyoPanel.useOriginal();
			}
		});

		final CoordinatesPresets presets = getCoordinatesPresets();
		int nPresets = presets.getNumPresets();
		presetButtons = new JButton[nPresets];
		for (int i = 0; i < nPresets; i++) {
			JButton presetButton = new JButton((presets.getPresetNames()) [i]);
			final int index = i;
			presetButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					switchFileset(presets.getFileSet(index));
				}
			});
			presetButtons[index] = presetButton;
		}
		fileButton = new JButton("Edit Presets...");
		fileButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SettingsPanel presetEditor = KaryoscopeFactory.getCoordinatesPresetsEditor();
				JDialog popup = new JDialog(getFrame(), "Edit Coordinates Presets");
				SettingsPanelHolder holder = new SettingsPanelHolder(popup, getFrame().getApp().getGlobalConfig().getRoot());
				holder.addSettingsPanel(presetEditor);
				popup.getContentPane().add(holder);
				popup.setModal(true);
				popup.pack();
				popup.setVisible(true);
				configureWidgets();
				addWidgets();
				revalidate();
				repaint();
			}
		});
	}
	private void switchFileset(FileSet fileSet1) {
		try { 
			setEnabled(false);
			karyoPanel.getGenome(fileSet1);
		} catch (LoadException ex) {
			setEnabled(true);
			LogBuffer.println("CoordinatesSettingsPanel got error" + ex.toString());
			JOptionPane.showMessageDialog(getFrame(), ex.toString(), "Load Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	public void synchronizeTo() {
		
	}
	
	public void synchronizeFrom() {
	}

}
