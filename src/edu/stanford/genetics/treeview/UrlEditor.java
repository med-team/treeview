/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: UrlEditor.java,v $
 * $Revision: 1.4 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
/**
 * This class extracts Urls from HeaderInfo.
 * Also included is a class to pop up a configuration window.
 */
public class UrlEditor {
    private UrlExtractor extractor;
    private UrlPresets presets;
    private Window window;
    private HeaderInfo headerInfo;

    /**
     * This class must be constructed around a HeaderInfo
     */
    public UrlEditor(UrlExtractor ue, UrlPresets up, HeaderInfo hI) {
	  super();
	  extractor = ue;
	  presets = up;
	  headerInfo = hI;
    }
    
    /**
     * pops up a configuration dialog.
     */
    public void showConfig(Frame f) {
	if (window == null) {
	    Dialog d = new Dialog(f, getTitle(), false);
	    d.setLayout(new BorderLayout());
	    d.add(new UrlEditPanel());
	    d.add(new JLabel(getTitle()), BorderLayout.NORTH);
	    d.add(new ButtonPanel(), BorderLayout.SOUTH);
	    d.addWindowListener(new WindowAdapter (){
		    public void windowClosing(WindowEvent we) 
		    {we.getWindow().setVisible(false);}
		});
	    d.pack();
	    window = d;
	}
	window.setVisible(true);
    }

    public static void main(String [] argv) {
	UrlPresets p = new UrlPresets(new DummyConfigNode("UrlPresets"));
	UrlEditor e  = new UrlEditor(new UrlExtractor(null), 
				     p,
				     null);
	Frame f = new Frame(getTitle());
	e.addToFrame(f);

	f.addWindowListener(new WindowAdapter (){
		public void windowClosing(WindowEvent we) 
		{System.exit(0);}
	    });
	f.pack();
	f.setVisible(true);
    }

    public void addToFrame(Frame f) {
	f.setLayout(new BorderLayout());
	f.add(new UrlEditPanel());
	//	f.add(new Label(getTitle(),Label.CENTER), BorderLayout.NORTH);
	f.add(new ButtonPanel(), BorderLayout.SOUTH);
	window = f;
    }

    private static String getTitle() {return "Url Link Editor";}

    //inner classes
    private class ButtonPanel extends Panel {
	ButtonPanel() {
	    JButton close_button = new JButton("Close");
	    close_button.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			window.dispose();
		    }
		});
	    add(close_button);
	}
    }    
    private class UrlEditPanel extends Panel {
	  UrlEditPanel () {
		redoLayout();
		templateField.setText(extractor.getUrlTemplate());
		headerChoice.select(extractor.getIndex());
		updatePreview();
	  }
	  private GridBagConstraints gbc;
	  public void redoLayout() {
		  String [] preset;
		  preset = presets.getPresetNames();
		  int nPresets = preset.length;
		  removeAll();
		  setLayout(new GridBagLayout());
		  gbc = new GridBagConstraints();
		  gbc.weighty = 100;
		  gbc.gridwidth = 1;
		  gbc.fill = GridBagConstraints.HORIZONTAL;
		  gbc.anchor = GridBagConstraints.NORTH;
		  gbc.gridy = 0;
		  gbc.weighty = 0;
		  addTemplate();
		  gbc.gridy  = 1;
		  addHeader();
		  gbc.gridy = 2;
		  addPreview();
		  gbc.gridy = 3;
		  gbc.gridx = 0;
		  gbc.gridwidth = 3;
		  gbc.weighty = 100;
		  add(new JLabel("Url Presets (Can edit under Program Menu)", JLabel.CENTER), gbc);
		  gbc.gridwidth = 1;
		  for (int i = 0; i < nPresets; i++) {
		  gbc.gridy++;
			addPreset(i);
		  }
	  	}

		String tester = "YAL039W";
		JTextField previewField;
		private void addPreview() {
		  gbc.gridx = 0;
		  gbc.weightx = 0;
		  add(new JLabel("Preview:"), gbc);
		  gbc.gridx = 1;
		  gbc.weightx = 100;
		  previewField = new JTextField(extractor.substitute(tester));
		  previewField.setEditable(false);
		  add(previewField, gbc);
		  JButton update= new JButton("Update");
		  update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		    updatePreview();
			}
		  });
		  gbc.gridx = 2;
		  gbc.weightx = 0;
//		  add(update, gbc);
		}

		private void updatePreview() {
		  extractor.setUrlTemplate(templateField.getText());
		  extractor.setIndex(headerChoice.getSelectedIndex());
		  previewField.setText(extractor.getUrl(0));
		}

		private HeaderChoice headerChoice;
		private void addHeader() {
		  gbc.gridx = 0;
		  gbc.weightx = 0;
		  add(new JLabel("Header:"), gbc);
		  gbc.gridx = 1;
		  gbc.weightx = 100;
		  headerChoice  = new HeaderChoice();
		  add(headerChoice, gbc);
		}



    private class HeaderChoice extends Choice implements ItemListener {
	  HeaderChoice() {
	    super();
	    String [] headers;
		int lastI;
	    if (headerInfo != null) {
		  headers = headerInfo.getNames();
		  lastI = headers.length;
		  if (headerInfo.getIndex("GWEIGHT") != -1) {
			lastI--;
		  }
	    } else {
		  headers = new String [] {"Dummy1", "Dummy2", "Dummy3"};
		  lastI = headers.length;
	    }

	    for (int i = 0; i < lastI; i++) {
		  add(headers[i]);
	    }
	    addItemListener(this);
	  }
	  public void itemStateChanged(ItemEvent e) {
	    updatePreview();
	  }
    }
    


		private TemplateField templateField;
		private void addTemplate() {
		  gbc.gridx = 0;
		  gbc.weightx = 0;
		  add(new JLabel("Template:"), gbc);
		  gbc.gridx = 1;
		  gbc.weightx = 100;
		  templateField = new TemplateField();
		  add(templateField, gbc);
		  JButton updateButton = new JButton ("Update");
		  updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  updatePreview();
			}
		  });
		  gbc.gridx = 2;
		  gbc.weightx = 0;
		  add(updateButton, gbc);
		}
		private class TemplateField extends TextField {
		  TemplateField () {
			super("enter url template");
			addActionListener(new ActionListener() {
		    	public void actionPerformed(ActionEvent e) {
				  updatePreview();
				}
			});
		  }
		}

		private void addPreset(int i) {
		  final int index = i;
		  gbc.gridx = 0;
		  add(new JLabel((presets.getPresetNames()) [index]), gbc);
		  gbc.gridx = 1;
		  gbc.weightx = 100;
		  add(new JTextField(presets.getTemplate(index)), gbc);
		  gbc.gridx = 2;
		  gbc.weightx = 0;
		  JButton set = new JButton("Set");
		  set.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  templateField.setText(presets.getTemplate(index));
			  updatePreview();
			}
		  });
		  add(set, gbc);
		}
	}
}

