/* BEGIN_HEADER                                              Java TreeView
 *
 * $Author: alokito $
 * $RCSfile: ColorSetI.java,v $
 * $Revision: 1.4 $
 * $Date: 2004-12-21 03:28:14 $
 * $Name:  $
 *
 * This file is part of Java TreeView
 * Copyright (C) 2001-2014 Alok Saldanha, All Rights Reserved. Modifications Copyright (C) Lawrence Berkeley Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * END_HEADER
 */
package edu.stanford.genetics.treeview;

import java.awt.*;

/**
 *  This is a general interface to color sets. This should be subclassed for particular
 *  color sets. Other classes, such as presets, which want to configure or get colors
 *  can do it through this interface in a general way. They can also down-cast if
 *  they need to. In general, the types should be constants within the subclass.
 *  See an existing example to get the idea.
 *
 * @author     Alok Saldanha <alok@genome.stanford.edu>
 * @version    @version $Revision: 1.4 $ $Date: 2004-12-21 03:28:14 $
 */
public interface ColorSetI {
	/**
	 *  textual descriptions of the types of colors in this set
	 */
	public abstract String[] getTypes();


	/**
	 *  get the color corresponding to the i'th type.
	 *
	 * @param  i  type index
	 * @return    The actual color
	 */
	public abstract Color getColor(int i);


	/**
	 *  set the i'th color to something new.
	 *
	 * @param  i   index of the type
	 * @param  newColor   The new color
	 */
	public abstract void setColor(int i, Color newColor);


	/**
	 *  get the description of the i'th type
	 *
	 * @param  i   index of the type
	 * @return    The description of the type
	 */
	public abstract String getType(int i);


	/**
	 *  get the name of the color set
	 *
	 * @return    Usually the name of the subclass.
	 */
	public String getName();
}

